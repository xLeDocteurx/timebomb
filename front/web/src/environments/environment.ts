// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  APIS_BASE_URL_BACK: 'http://localhost:1337',
  APIS_BASE_URL_SOCKET: 'http://localhost:1338',
  APIS_BASE_URL_AUTH: 'http://localhost:1339',
  animations: {
    REVEAL_CARD_TIMEOUT: 3000,
    NEW_TURN_TIMEOUT: 3000,
    BOMB_EXPLODE_TIMEOUT: 3000
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
