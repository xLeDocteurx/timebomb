export const environment = {
  production: true,
  // APIS_BASE_URL_BACK: 'http://timebom.be:1337',
  APIS_BASE_URL_BACK: 'https://timebom.be/api',
  // APIS_BASE_URL_BACK: 'http://185.98.136.22:1337',

  // APIS_BASE_URL_SOCKET: 'http://timebom.be:1338',
  APIS_BASE_URL_SOCKET: 'https://timebom.be/socket',
  // APIS_BASE_URL_SOCKET: 'http://185.98.136.22:1338',

  // APIS_BASE_URL_AUTH: 'http://timebom.be:1339',
  APIS_BASE_URL_AUTH: 'https://timebom.be/auth',
  // APIS_BASE_URL_AUTH: 'http://185.98.136.22:1339',

  animations: {
    REVEAL_CARD_TIMEOUT: 3000,
    NEW_TURN_TIMEOUT: 3000,
    BOMB_EXPLODE_TIMEOUT: 3000
  }
};
