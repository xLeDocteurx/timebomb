import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class IsAdminGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    if (this.authService.isLoggedIn) {
      const user = await this.authService.getUser();
      if (user.role === RoleType.professional) {
        return true;
      } else {
        this.router.navigateByUrl('home');
        return false;
      }
    } else {
      this.router.navigateByUrl('login');
      return false;
    }
  }
}
