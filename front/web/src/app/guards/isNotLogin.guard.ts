import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class IsNotLoginGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    if (!this.authService.isLoggedIn) {
      return true;
    } else {
      // const user = await this.authService.getUser();
      this.router.navigateByUrl('profile');
    }
  }
}
