import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameModule } from './pages/game/game.module';
import { HomeModule } from './pages/home/home.module';
import { HeaderComponent } from './sharedComponents/header/header.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    GameModule
    // CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
