import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PrivateUser } from '../../../../../../models/interfaces';
import { AuthService } from '../../services';
import { NetworkLoadingService } from '../../services/network.loading/network.loading.service';

@Component({
  selector: 'tb-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  $authObs: Observable<PrivateUser>;

  constructor(
    public loadingService: NetworkLoadingService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.$authObs = this.authService.subject;

    // console.log('this.route', this.route);
  }

  get isLoggedIn() {
    return this.authService.isLoggedIn;
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/');
  }
}
