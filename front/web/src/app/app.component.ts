import { Component, OnInit } from '@angular/core';
import { AuthService, SocketService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'web';

  constructor(
    private socketService: SocketService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    // console.log('this.socketService.socket', this.socketService.socket);
  }
}
