import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { IsNotLoginGuard } from './guards';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' },
  {
    path: 'login',
    // canActivate: [IsNotLoginGuard],
    loadChildren: () =>
      import('./pages/login/login.routing').then(m => m.LoginRoutingModule)
  },
  {
    path: 'register',
    // canActivate: [IsNotLoginGuard],
    loadChildren: () =>
      import('./pages/register/register.routing').then(
        m => m.RegisterRoutingModule
      )
  },
  {
    path: 'profile',
    // canActivate: [IsLoginGuard],
    loadChildren: () =>
      import('./pages/profile/profile.routing').then(
        m => m.ProfileRoutingModule
      )
  },
  {
    path: 'lobby',
    // canActivate: [IsLoginGuard],
    loadChildren: () =>
      import('./pages/lobby/lobby.routing').then(m => m.LobbyRoutingModule)
  },
  {
    path: 'games/:slug',
    // path: 'games',
    // canActivate: [IsLoginGuard, IsValidSlug],
    loadChildren: () =>
      import('./pages/game/game.routing').then(m => m.GameRoutingModule)
  },
  { path: '**', redirectTo: '/login' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes
      // {
      // useHash: environment.useAngularHashTag
      // }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
  constructor() {
    console.log('activated app routing');
  }
}
