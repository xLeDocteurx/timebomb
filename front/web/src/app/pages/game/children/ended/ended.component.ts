import { Component, Input, OnInit } from '@angular/core';
import { PublicGame } from '../../../../../../../../models/interfaces';

@Component({
  selector: 'app-ended',
  templateUrl: './ended.component.html',
  styleUrls: ['./ended.component.scss']
})
export class EndedComponent implements OnInit {
  @Input() game: PublicGame;

  constructor() {}

  ngOnInit() {}
}
