import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PublicGame } from '../../../../../../../../models/interfaces';
import { session } from '../../../../../../../../utils';
import { ErrorService, GameService, SocketService } from '../../../../services';

@Component({
  selector: 'app-open',
  templateUrl: './open.component.html',
  styleUrls: ['./open.component.scss']
})
export class OpenComponent implements OnInit {
  @Input() game: PublicGame;

  constructor(
    private gameService: GameService,
    private socketService: SocketService,
    private router: Router,
    private errorService: ErrorService
  ) {}

  ngOnInit() {}

  async launchGame() {
    try {
      await this.gameService.launchGame(this.game.slug);
    } catch (err) {
      this.errorService.throwError(err);
    }
  }

  async joinGame() {
    try {
      await this.gameService.joinGame(this.game.slug);
      // this.socketService.joinGame(this.game.slug);
    } catch (err) {
      this.errorService.throwError(err);
    }
  }

  async leaveGame() {
    try {
      await this.gameService.leaveGame(this.game.slug);
    } catch (err) {
      this.errorService.throwError(err);
    }
  }

  navigateTo(path: string) {
    this.router.navigateByUrl(path);
  }

  gravatarUrl(user) {
    return session.getGravatarUrl(user);
  }
}
