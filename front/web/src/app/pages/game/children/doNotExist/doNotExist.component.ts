import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doNotExist',
  templateUrl: './doNotExist.component.html',
  styleUrls: ['./doNotExist.component.scss']
})
export class DoNotExistComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
