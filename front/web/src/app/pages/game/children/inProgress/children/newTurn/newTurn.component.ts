import { Component, Input, OnInit } from '@angular/core';

export interface NewTurnData {
  turnNumber: number;
}

@Component({
  selector: 'app-newTurn',
  templateUrl: './newTurn.component.html',
  styleUrls: ['./newTurn.component.scss']
})
export class NewTurnComponent implements OnInit {
  @Input() data: NewTurnData;

  constructor() {}

  ngOnInit() {}
}
