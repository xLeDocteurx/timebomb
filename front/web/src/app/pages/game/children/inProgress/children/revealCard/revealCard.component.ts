import { Component, Input, OnInit } from '@angular/core';
import { CardsTypes } from '../../../../../../../../../../models/enums';

export interface RevealCardData {
  cardType: CardsTypes;
}

@Component({
  selector: 'app-revealCard',
  templateUrl: './revealCard.component.html',
  styleUrls: ['./revealCard.component.scss']
})
export class RevealCardComponent implements OnInit {
  @Input() data: RevealCardData;

  constructor() {}

  ngOnInit() {
    console.log('revealCardComponent this.data : ', this.data);
  }
}
