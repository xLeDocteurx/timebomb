/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RevealCardComponent } from './revealCard.component';

describe('RevealCardComponent', () => {
  let component: RevealCardComponent;
  let fixture: ComponentFixture<RevealCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevealCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevealCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
