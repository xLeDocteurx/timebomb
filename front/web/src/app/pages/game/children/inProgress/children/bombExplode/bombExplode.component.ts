import { Component, Input, OnInit } from '@angular/core';

export interface BombExplodeData {}

@Component({
  selector: 'app-bombExplode',
  templateUrl: './bombExplode.component.html',
  styleUrls: ['./bombExplode.component.scss']
})
export class BombExplodeComponent implements OnInit {
  @Input() data: BombExplodeData;

  constructor() {}

  ngOnInit() {}
}
