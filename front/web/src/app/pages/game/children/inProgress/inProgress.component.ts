import { Component, ComponentFactoryResolver, Input, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { ObjectId } from 'mongodb';
import { CardsTypes } from '../../../../../../../../models/enums';
import { CardRevealedResponse, PrivateUser, PublicGame, PublicUser } from '../../../../../../../../models/interfaces';
import { omit, session } from '../../../../../../../../utils';
import { environment } from '../../../../../environments/environment';
// import { RevealCardDirective } from '../../../../directives';
import { AuthService, ErrorService, GameService, SocketService } from '../../../../services';
import { BombExplodeComponent } from './children/bombExplode/bombExplode.component';
import { NewTurnComponent } from './children/newTurn/newTurn.component';
import { RevealCardComponent } from './children/revealCard/revealCard.component';

@Component({
  selector: 'app-inProgress',
  templateUrl: './inProgress.component.html',
  styleUrls: ['./inProgress.component.scss']
})
export class InProgressComponent implements OnInit {
  @Input() game: PublicGame;
  @Input() user: PrivateUser;
  Array = Array;

  // @ViewChild(RevealCardDirective, { static: true })
  // revealedCard: RevealCardDirective;
  // @ViewChild('revealedCard', { static: true })
  // revealedCard: RevealCardDirective;
  @ViewChild('revealedCard', { read: ViewContainerRef })
  revealedCard: ViewContainerRef;
  // @ViewChild('revealedCard') revealedCard: ElementRef;
  @ViewChild('newTurn', { read: ViewContainerRef })
  newTurn: ViewContainerRef;
  @ViewChild('bombExplode', { read: ViewContainerRef })
  bombExplode: ViewContainerRef;

  constructor(
    private gameService: GameService,
    private errorService: ErrorService,
    private authService: AuthService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private socketService: SocketService
  ) {}

  get playerToPlay() {
    return this.game.players.find(player => {
      return player._id === this.game.state.playerToPlay;
    });
  }

  get currentStatePlayer() {
    return this.game.state.players[this.user._id.toString()];
  }

  get otherStatePlayers() {
    // const otherStatePlayers = clone(this.game.state.players);
    // delete otherStatePlayers[this.user._id.toString()];
    return omit(this.game.state.players, [this.user._id.toString()]);
  }

  ngOnInit() {
    this.socketService.registerSocketEvent<CardRevealedResponse>('cardRevealed', data => {
      this.revealCardAnimation(data);
    });
    // protected registerSocketEvent(event: string, callback: (...args) => void) {
    //   this.socketService.socket.on(event, callback);
    // }
  }

  async revealCard(playerId: string, cardIndex: number) {
    try {
      await this.gameService.revealCard(this.game.slug, playerId, cardIndex);
    } catch (err) {
      this.errorService.throwError(err);
    }
  }

  getPlayer(id: string | ObjectId) {
    return this.game.players.find(player => {
      return player._id === id;
    });
  }

  gravatarUrl(user: PublicUser) {
    return session.getGravatarUrl(user);
  }

  revealCardAnimation(data: CardRevealedResponse) {
    const { cardType, cardIndex } = data;
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(RevealCardComponent);

    // const viewContainerRef = this.revealedCard.viewContainerRef;
    const viewContainerRef = this.revealedCard;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<RevealCardComponent>(componentFactory);
    componentRef.instance.data = { cardType: cardType as CardsTypes };

    setTimeout(() => {
      viewContainerRef.clear();
      if (cardType === CardsTypes.bomb) {
        this.bombExplodeAnimation();
      } else if (this.game.state.newTurn) {
        this.newTurnAnimation(this.game.state.turn);
      }
    }, environment.animations.REVEAL_CARD_TIMEOUT);
  }

  newTurnAnimation(turnNumber: number) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(NewTurnComponent);

    // const viewContainerRef = this.newTurn.viewContainerRef;
    const viewContainerRef = this.newTurn;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<NewTurnComponent>(componentFactory);
    componentRef.instance.data = { turnNumber: turnNumber };

    setTimeout(() => {
      viewContainerRef.clear();
    }, environment.animations.NEW_TURN_TIMEOUT);
  }

  bombExplodeAnimation() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(BombExplodeComponent);

    // const viewContainerRef = this.bombExplode.viewContainerRef;
    const viewContainerRef = this.bombExplode;
    viewContainerRef.clear();

    const componentRef = viewContainerRef.createComponent<BombExplodeComponent>(componentFactory);
    componentRef.instance.data = {};

    setTimeout(() => {
      viewContainerRef.clear();
    }, environment.animations.BOMB_EXPLODE_TIMEOUT);
  }
}
