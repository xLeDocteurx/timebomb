/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BombExplodeComponent } from './bombExplode.component';

describe('BombExplodeComponent', () => {
  let component: BombExplodeComponent;
  let fixture: ComponentFixture<BombExplodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BombExplodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BombExplodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
