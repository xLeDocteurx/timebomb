import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DoNotExistComponent } from './children/doNotExist/doNotExist.component';
import { EndedComponent } from './children/ended/ended.component';
import { BombExplodeComponent } from './children/inProgress/children/bombExplode/bombExplode.component';
import { NewTurnComponent } from './children/inProgress/children/newTurn/newTurn.component';
import { RevealCardComponent } from './children/inProgress/children/revealCard/revealCard.component';
import { InProgressComponent } from './children/inProgress/inProgress.component';
import { OpenComponent } from './children/open/open.component';
import { GameComponent } from './game.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [
    GameComponent,
    OpenComponent,
    InProgressComponent,
    EndedComponent,
    DoNotExistComponent,
    NewTurnComponent,
    BombExplodeComponent,
    RevealCardComponent
  ]
})
export class GameModule {}
