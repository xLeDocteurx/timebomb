import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GameComponent } from './game.component';
import { GameModule } from './game.module';

const routeList: Routes = [
  {
    path: '',
    // path: ':slug',
    component: GameComponent
  }
];

@NgModule({
  imports: [GameModule, RouterModule.forChild(routeList)]
})
export class GameRoutingModule {
  constructor() {
    console.log('activated game routing');
  }
}
