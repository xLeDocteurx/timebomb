import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import {
  PrivateUser,
  PublicGame,
  SocketIncomingMessage,
  SocketOutgoingMessage
} from '../../../../../../models/interfaces';
import {
  AuthService,
  ChatService,
  GameService,
  SocketService
} from '../../services/';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, OnDestroy {
  slug: string;
  $user: Observable<PrivateUser>;
  $currentGame: Observable<PublicGame>;

  // @ViewChild('messages', { read: ViewContainerRef }) messages: ViewContainerRef;
  // @ViewChild('messages') messagesElementRef: ElementRef;
  messagesSubscription: Subscription;
  messages: SocketIncomingMessage[] = [];
  message: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private elementRef: ElementRef,
    private renderer: Renderer2,
    // private resolver: ComponentFactoryResolver,
    private authService: AuthService,
    private gameService: GameService,
    private socketService: SocketService,
    private chatService: ChatService
  ) {}

  ngOnInit() {
    this.slug = this.route.snapshot.params['slug'];
    this.gameService.getGame(this.slug);

    this.$user = this.authService.subject;
    this.$currentGame = this.gameService.$currentGame;

    this.messagesSubscription = this.chatService.$incomingMessages.subscribe(
      message => {
        if (message) this.messages.push(message);
      }
    );
  }

  ngOnDestroy() {
    this.messagesSubscription.unsubscribe();
  }

  sendMessage() {
    const message: SocketOutgoingMessage = {
      content: this.message
    };

    this.messages.push({ sender: 'You', content: message.content });
    this.chatService.sendMessage(this.slug, this.message);

    this.message = '';
  }
}
