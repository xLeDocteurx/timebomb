import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register.component';
import { RegisterModule } from './register.module';

const routeList: Routes = [
  {
    path: '',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RegisterModule, RouterModule.forChild(routeList)]
})
export class RegisterRoutingModule {}
