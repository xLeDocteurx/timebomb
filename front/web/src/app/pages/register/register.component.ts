import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as md5 from 'js-md5';
import { RegExpDic } from '../../../../../../models/dics';
import { AvatarTypes } from '../../../../../../models/enums';
import { AuthService, ErrorService } from '../../services';

@Component({
  selector: 'tb-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  avatarTypes = AvatarTypes;
  get avatarTypesKeys() {
    return Object.keys(this.avatarTypes);
  }

  constructor(
    private authService: AuthService,
    private errorService: ErrorService,
    private router: Router
  ) {}
  // constructor() {}

  async ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      username: new FormControl(''),
      email: new FormControl('', Validators.email),
      password1: new FormControl('', Validators.pattern(RegExpDic.password)),
      password2: new FormControl('', Validators.pattern(RegExpDic.password)),
      avatarType: new FormControl(AvatarTypes.robohash)
    });
  }

  get canSubmit() {
    if (
      this.form.getRawValue()['username'] &&
      this.form.getRawValue()['email'] &&
      this.form.getRawValue()['password1'] &&
      this.form.getRawValue()['password2'] &&
      this.form.getRawValue()['avatarType'] &&
      this.form.getRawValue()['password1'] ===
        this.form.getRawValue()['password2']
    ) {
      return true;
    } else {
      return false;
    }
  }

  async submit() {
    try {
      await this.authService.register(this.form.getRawValue());
      this.router.navigateByUrl('/');
    } catch (err) {
      console.log('err', err);
      this.errorService.throwError(err);
    }
  }

  getGravatarUrl() {
    const gravatarHash = md5(this.form.getRawValue()['email'].toLowerCase());
    const avatarType = this.form.getRawValue()['avatarType'];

    if (this.form.getRawValue()['avatarType'] === AvatarTypes.gravatar) {
      return `https://www.gravatar.com/avatar/${gravatarHash}`;
    } else {
      return `https://www.gravatar.com/avatar/${gravatarHash}?&d=${avatarType}`;
    }
  }
}
