import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService, ErrorService } from '../../services';

@Component({
  selector: 'tb-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService, private errorService: ErrorService, private router: Router) {}
  // constructor() {}

  async ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  get canSubmit() {
    if (this.form.getRawValue()['email'] && this.form.getRawValue()['password']) {
      return true;
    } else {
      return false;
    }
  }

  async submit() {
    try {
      await this.authService.login(this.form.getRawValue());
      this.router.navigateByUrl('/');
    } catch (err) {
      console.log('err', err);
      this.errorService.throwError(err);
    }
  }
}
