import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginModule } from './login.module';

const routeList: Routes = [
  {
    path: '',
    component: LoginComponent
  }
];

@NgModule({
  imports: [LoginModule, RouterModule.forChild(routeList)]
})
export class LoginRoutingModule {}
