import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { AuthenticatedComponent } from './sub.components/authenticated/authenticated.component';
import { VisitorsComponent } from './sub.components/visitors/visitors.component';

@NgModule({
  imports: [CommonModule],
  declarations: [HomeComponent, AuthenticatedComponent, VisitorsComponent]
})
export class HomeModule {}
