import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { ProfileModule } from './profile.module';

const routeList: Routes = [
  {
    path: '',
    component: ProfileComponent
  }
];

@NgModule({
  imports: [ProfileModule, RouterModule.forChild(routeList)]
})
export class ProfileRoutingModule {}
