import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { PrivateUser } from '../../../../../../models/interfaces';
import { session } from '../../../../../../utils';
import { AuthService } from '../../services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  $authObs: Observable<PrivateUser>;
  authSubscription: Subscription;
  authValue: PrivateUser;

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.$authObs = this.authService.subject;

    this.authSubscription = this.authService.subject.subscribe(value => {
      console.log('profile value : ', value);
      this.authValue = value;
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }

  gravatarUrl(user) {
    return session.getGravatarUrl(user);
  }
}
