import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { LobbyComponent } from './lobby.component';
@NgModule({
  imports: [CommonModule, ReactiveFormsModule],
  declarations: [LobbyComponent]
})
export class LobbyModule {}
