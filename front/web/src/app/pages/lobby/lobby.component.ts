import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { PrivateUser, PublicGame } from '../../../../../../models/interfaces';
import { AuthService, ErrorService, LobbyService } from '../../services';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.scss']
})
export class LobbyComponent implements OnInit, OnDestroy {
  $lobbyObs: Observable<PublicGame[]>;
  authSubscription: Subscription;
  authValue: PrivateUser;

  _showCreateGame: boolean = false;
  createGameForm: FormGroup;

  constructor(
    private authService: AuthService,
    private lobbyService: LobbyService,
    private errorService: ErrorService,
    private router: Router
  ) {}

  get showCreateGame() {
    return this._showCreateGame;
  }

  showCreateGameForm() {
    this.initCreateGameForm();
    this._showCreateGame = true;
  }

  hideCreateGameForm() {
    this._showCreateGame = false;
  }

  ngOnInit() {
    this.$lobbyObs = this.lobbyService.subject; /*.pipe(filter());*/
    this.lobbyService.subject.subscribe(value => {
      console.log('lobby value : ', value);
    });
    this.authSubscription = this.authService.subject.subscribe(value => {
      this.authValue = value;
    });
  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
  }

  get canCreateGame() {
    if (
      this.createGameForm.getRawValue()['name'] &&
      !this.authValue.currentGame
    ) {
      return true;
    } else {
      return false;
    }
  }

  initCreateGameForm() {
    this.createGameForm = new FormGroup({
      name: new FormControl('')
    });
  }

  async createGame() {
    try {
      await this.lobbyService.createAGame({
        options: this.createGameForm.getRawValue()
      });
      this.hideCreateGameForm();
      // this.authService.refetch();
      // await this.openGame(this.authValue.currentGame.slug);
    } catch (err) {
      console.log('err', err);
      this.errorService.throwError(err);
    }
  }

  async openGame(slug: string) {
    await this.router.navigateByUrl(`games/${slug}`);
  }
}
