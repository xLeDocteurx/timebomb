import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LobbyComponent } from './lobby.component';
import { LobbyModule } from './lobby.module';

const routeList: Routes = [
  {
    path: '',
    component: LobbyComponent
  }
];

@NgModule({
  imports: [LobbyModule, RouterModule.forChild(routeList)]
})
export class LobbyRoutingModule {}
