import { Injectable } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkLoadingService {
  private _isLoading: BehaviorSubject<number>;
  private _loadingElementsNumber: number = 0;

  networkSubsciptions: Subscription[];

  get isLoading() {
    return this._isLoading.asObservable();
  }
  get loadingElementsNumber() {
    return this._loadingElementsNumber;
  }

  constructor() {}

  // subscribe(observable: Observable<boolean>) {
  // subscribe(observable: Boolean) {}
  subscribe() {
    this._loadingElementsNumber += 1;
  }

  unsubscribe() {
    this._loadingElementsNumber -= 1;
  }
}
