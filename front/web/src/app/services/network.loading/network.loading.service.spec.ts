/* tslint:disable:no-unused-variable */

import { inject, TestBed } from '@angular/core/testing';
import { NetworkLoadingService } from './network.loading.service';

describe('Service: Network.loading', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NetworkLoadingService]
    });
  });

  it('should ...', inject(
    [NetworkLoadingService],
    (service: NetworkLoadingService) => {
      expect(service).toBeTruthy();
    }
  ));
});
