import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIS, APISMethods } from '../../../../../../models/enums';
import { APIRequest, Dic, PublicGame } from '../../../../../../models/interfaces';
import { NetworkLoadingService } from '../network.loading/network.loading.service';
import { SocketService } from '../socket/socket.service';
import { GameServiceNetwork } from './game.service.network';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  // games: Dic<PublicGame> = {};
  games: Dic<GameServiceNetwork> = {};
  currentGameSlug: string;
  $currentGame: Observable<PublicGame>;

  constructor(private loadingService: NetworkLoadingService, private socketService: SocketService) {}

  getGame(slug: string) {
    const request: APIRequest = {
      target: APIS.back,
      method: APISMethods.get,
      url: `/games/${slug}`
    };

    this.games[slug] = new GameServiceNetwork(slug, request, this.loadingService, this.socketService);
    this.currentGameSlug = slug;
    this.$currentGame = this.games[slug].subject;
  }

  async launchGame(slug: string) {
    await this.games[slug].launchGame();
  }

  async joinGame(slug: string) {
    await this.games[slug].joinGame();
  }

  async leaveGame(slug: string) {
    await this.games[slug].leaveGame();
  }

  async revealCard(slug: string, playerId: string, cardIndex: number) {
    await this.games[slug].revealCard(playerId, cardIndex);
  }
}
