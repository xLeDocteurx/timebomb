import { Injectable } from '@angular/core';
import { APIS, APISMethods } from '../../../../../../models/enums';
import {
  APIRequest,
  CardRevealedResponse,
  GameLaunchedResponse,
  JoinGameRequestData,
  LaunchGameRequestData,
  PlayerJoinedResponse,
  PlayerLeavedResponse,
  PublicGame,
  RevealCardRequestData,
  SocketRequest
} from '../../../../../../models/interfaces';
import { environment } from '../../../environments/environment';
import { NetworkService } from '../network.class/network.class.service';
import { NetworkLoadingService } from '../network.loading/network.loading.service';
import { SocketService } from '../socket/socket.service';

interface Params {}

interface Results {}

@Injectable({
  providedIn: 'root'
})
export class GameServiceNetwork extends NetworkService<Params, PublicGame> {
  slug: string;
  // socketService: SocketService;
  constructor(slug: string, request: APIRequest, loadingService: NetworkLoadingService, socketService: SocketService) {
    super(request, loadingService, socketService);
    this.slug = slug;
    this.socketService.registerSocketEvent<PlayerJoinedResponse>('playerJoined', data => {
      console.log('playerJoined');
      this.refetch();
    });
    this.socketService.registerSocketEvent<PlayerLeavedResponse>('playerLeaved', data => {
      console.log('playerLeaved');
      this.refetch();
    });
    this.socketService.registerSocketEvent<GameLaunchedResponse>('gameLaunched', data => {
      console.log('gameLaunched');
      this.refetch();
    });
    this.socketService.registerSocketEvent<CardRevealedResponse>('cardRevealed', data => {
      console.log('data : ', data);
      setTimeout(() => {
        this.refetch();
      }, environment.animations.REVEAL_CARD_TIMEOUT);
    });
  }

  map(results) {
    return results;
  }

  async launchGame() {
    const request: SocketRequest<LaunchGameRequestData> = {
      event: 'launchGame',
      payload: {
        data: {
          slug: this.slug
        }
      }
    };
    this.emitSocketEvent<LaunchGameRequestData>(request);
    // const request: APIRequest = {
    //   target: APIS.back,
    //   method: APISMethods.post,
    //   url: '/games/launch'
    // };
    // const res = await this.mutate<boolean>(request, { slug: this.slug });
    // this.refetch();
    // return res;
  }

  async joinGame() {
    const request: SocketRequest<JoinGameRequestData> = {
      event: 'joinGame',
      payload: {
        data: {
          slug: this.slug
        }
      }
    };
    this.emitSocketEvent<JoinGameRequestData>(request);
    // const request: APIRequest = {
    //   target: APIS.back,
    //   method: APISMethods.post,
    //   url: '/games/join'
    // };
    // const res = await this.mutate<boolean>(request, { slug: this.slug });
    // this.refetch();
    // return res;
  }

  async leaveGame() {
    const request: APIRequest = {
      target: APIS.back,
      method: APISMethods.post,
      url: '/games/leave'
    };
    const res = await this.mutate<boolean, { slug: string }>(request, { slug: this.slug });
    this.refetch();
    return res;
  }

  async revealCard(playerId: string, cardIndex: number) {
    const request: SocketRequest<RevealCardRequestData> = {
      event: 'revealCard',
      payload: {
        data: {
          slug: this.slug,
          playerId: playerId,
          cardIndex: cardIndex
        }
      }
    };
    this.emitSocketEvent<RevealCardRequestData>(request);
    // const request: APIRequest = {
    //   target: APIS.back,
    //   method: APISMethods.post,
    //   url: '/game/revealCard'
    // };
    // const res = await this.mutate<boolean>(request, { playerId });
    // this.refetch();
    // return res;
  }

  // cardRevealed
}
