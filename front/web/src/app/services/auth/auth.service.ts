import { Injectable } from '@angular/core';
import { APIS, APISMethods } from '../../../../../../models/enums';
import {
  APIRequest,
  ConnectResponse,
  GetPasswordAndPrivateKeySaltsResponse,
  GetPasswordSaltsAndChallengeInput,
  GetPasswordSaltsAndChallengeResponse,
  PrivateUser,
  RegisterResponse,
  UserInputConnect,
  UserInputConnectLight,
  UserInputRegister,
  UserInputRegisterLight
} from '../../../../../../models/interfaces';
import { h, hmac, hsecAsync, KeyPair, session, SymKey, xor } from '../../../../../../utils';
import { NetworkService } from '../network.class/network.class.service';
import { NetworkLoadingService } from '../network.loading/network.loading.service';
import { SocketService } from '../socket/socket.service';

interface Params {}

interface Results {}

@Injectable({
  providedIn: 'root'
})
// export class AuthService extends NetworkService<Params, Results> {
export class AuthService extends NetworkService<Params, PrivateUser> {
  constructor(loadingService: NetworkLoadingService, socketService: SocketService) {
    const request: APIRequest = {
      target: APIS.auth,
      method: APISMethods.get,
      url: '/user'
    };

    super(request, loadingService, socketService);
  }

  map(results) {
    return results;
  }

  get isLoggedIn() {
    return session.getTokenAsCookie() ? true : false;
  }

  async register(userInputLight: UserInputRegisterLight) {
    const getPasswordAndPrivateKeySaltsRes = await this.mutate<GetPasswordAndPrivateKeySaltsResponse, null>({
      target: APIS.auth,
      method: APISMethods.get,
      url: '/register'
    });

    const keypair = new KeyPair();
    await keypair.generateKeyPair();

    const symKey = new SymKey({
      pem: await hsecAsync(
        userInputLight.password1,
        getPasswordAndPrivateKeySaltsRes.privateKeySalt.value,
        getPasswordAndPrivateKeySaltsRes.privateKeySalt.iterations,
        16
      )
    });

    const userInput: UserInputRegister = {
      username: userInputLight.username,
      email: userInputLight.email,
      avatarType: userInputLight.avatarType,
      passwordHash:
        // h(
        await hsecAsync(
          userInputLight.password1,
          getPasswordAndPrivateKeySaltsRes.passwordSalt.value,
          getPasswordAndPrivateKeySaltsRes.passwordSalt.iterations
        ),
      // getPasswordAndPrivateKeySaltsRes.passwordSalt.keyAlgs.hash
      // ),
      passwordSaltId: getPasswordAndPrivateKeySaltsRes.passwordSalt.redisId,
      publicKey: keypair.publicKeyPem,
      encryptedPrivate: await symKey.encrypt(keypair.privateKeyPem),
      privateKeySaltId: getPasswordAndPrivateKeySaltsRes.privateKeySalt.redisId
    };

    const res = await this.mutate<RegisterResponse, UserInputRegister>(
      {
        target: APIS.auth,
        method: APISMethods.post,
        url: '/register'
      },
      userInput
    );

    session.saveTokenAsCookie(res.token);
    this.init();
  }

  async login(userInputLight: UserInputConnectLight) {
    const getPasswordSaltsAndChallenge = await this.mutate<
      GetPasswordSaltsAndChallengeResponse,
      GetPasswordSaltsAndChallengeInput
    >(
      {
        target: APIS.auth,
        method: APISMethods.post,
        url: '/getchallenge'
      },
      { email: userInputLight.email }
    );

    const passwordHsec = await hsecAsync(
      userInputLight.password,
      getPasswordSaltsAndChallenge.passwordSalt.value,
      getPasswordSaltsAndChallenge.passwordSalt.iterations
    );

    const userInput: UserInputConnect = {
      email: userInputLight.email,
      passwordHash: xor(
        hmac(
          h(passwordHsec, getPasswordSaltsAndChallenge.passwordSalt.keyAlgs.hash),
          getPasswordSaltsAndChallenge.challenge.value,
          getPasswordSaltsAndChallenge.passwordSalt.keyAlgs.hmac
        ),
        passwordHsec
      ),
      passwordChallengeId: getPasswordSaltsAndChallenge.passwordSalt.redisId
    };

    const connectRes = await this.mutate<ConnectResponse, UserInputConnect>(
      {
        target: APIS.auth,
        method: APISMethods.post,
        url: '/connect'
      },
      userInput
    );
    session.saveTokenAsCookie(connectRes.token);

    const publicKey = connectRes.keys.public;
    const symKey = new SymKey({
      pem: await hsecAsync(
        userInputLight.password,
        connectRes.keys.encryptedPrivate.salt.value,
        connectRes.keys.encryptedPrivate.salt.iterations,
        16
      )
    });
    const privateKey = symKey.decrypt(connectRes.keys.encryptedPrivate.value);

    this.init();
  }

  async logout() {
    session.deleteAllCookies();
    // location.reload();
  }
}
