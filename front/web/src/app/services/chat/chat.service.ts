import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  SendMessageRequestData,
  SocketIncomingMessage,
  SocketOutgoingMessage,
  SocketRequest
} from '../../../../../../models/interfaces';
import { NetworkService } from '../network.class/network.class.service';
import { NetworkLoadingService } from '../network.loading/network.loading.service';
import { SocketService } from '../socket/socket.service';

interface Params {}

interface Results {}

@Injectable({
  providedIn: 'root'
})
export class ChatService extends NetworkService<Params, Results> {
  private _$incomingMessages: BehaviorSubject<SocketIncomingMessage> = new BehaviorSubject(null);

  constructor(loadingService: NetworkLoadingService, socketService: SocketService) {
    // const request: APIRequest = {
    //   target: APIS.auth,
    //   method: APISMethods.get,
    //   url: '/user'
    // };
    super(null, loadingService, socketService);

    // this.socketService.socket.on('receiveMessage', message => {
    //   this._$incomingMessages.next(message);
    // });
    this.socketService.registerSocketEvent<SocketIncomingMessage>('receiveMessage', message => {
      this._$incomingMessages.next(message);
    });
  }

  map(results) {
    console.log('Chatservice results : ', results);
    return results;
  }

  get $incomingMessages(): Observable<SocketIncomingMessage> {
    return this._$incomingMessages.asObservable();
  }

  sendMessage(slug: string, content: string) {
    const message: SocketOutgoingMessage = {
      content: content
    };
    const request: SocketRequest<SendMessageRequestData> = {
      event: 'sendMessage',
      payload: {
        data: {
          message: message,
          slug: slug
        }
      }
    };
    this.emitSocketEvent<SendMessageRequestData>(request);
  }
}
