import { Injectable } from '@angular/core';
import { APIS, APISMethods } from '../../../../../../models/enums';
import { APIRequest, GameInput, PublicGame } from '../../../../../../models/interfaces';
import { NetworkService } from '../network.class/network.class.service';
import { NetworkLoadingService } from '../network.loading/network.loading.service';
import { SocketService } from '../socket/socket.service';

interface Params {}

interface Results {}

@Injectable({
  providedIn: 'root'
})
export class LobbyService extends NetworkService<Params, PublicGame[]> {
  constructor(loadingService: NetworkLoadingService, socketService: SocketService) {
    const request: APIRequest = {
      target: APIS.back,
      method: APISMethods.get,
      url: '/games/availables'
    };

    super(request, loadingService, socketService);
  }

  map(results) {
    console.log('LobbyService results : ', results);
    return results;
  }

  async createAGame(gameInput: GameInput) {
    const res = await this.mutate<boolean, GameInput>(
      {
        target: APIS.back,
        method: APISMethods.post,
        url: '/games/create'
      },
      gameInput
    );
    this.refetch();
    return res;
  }
}
