import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  constructor() {}

  throwError(error) {
    alert(error);
  }
}
