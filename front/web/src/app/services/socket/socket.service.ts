import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { SocketRequest } from '../../../../../../models/interfaces';
import { session } from '../../../../../../utils';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  socket: SocketIOClient.Socket;

  constructor() {
    this.socket = io(environment.APIS_BASE_URL_SOCKET, { secure: true });
    this.initSocketConnection();
  }

  // registerSocketEvent(event: string, callback: (...args) => void) {
  registerSocketEvent<T>(event: string, callback: (args: T) => void) {
    this.socket.on(event, callback);
  }

  emit<T>(socketRequest: SocketRequest<T>) {
    socketRequest.payload.userToken = session.getTokenAsCookie();
    this.socket.emit(socketRequest.event, socketRequest.payload);
  }

  initSocketConnection<T>() {
    const request: SocketRequest<T> = {
      event: 'initSocketConnection',
      payload: {
        data: null
      }
    };
    this.emit(request);
  }
}
