import { Injectable } from '@angular/core';
import axios, { AxiosInstance, AxiosResponse } from 'axios';
import { BehaviorSubject, Observable } from 'rxjs';
import { APIS, APISMethods } from '../../../../../../models/enums';
import { APIRequest, SocketRequest } from '../../../../../../models/interfaces';
import { session } from '../../../../../../utils/';
import { environment } from '../../../environments/environment';
import { NetworkLoadingService } from '../network.loading/network.loading.service';
import { SocketService } from '../socket/socket.service';
import { mutate } from './mutate.function';

@Injectable({
  providedIn: 'root'
})
export abstract class NetworkService<Params, Results> {
  private _$subject: BehaviorSubject<any> = new BehaviorSubject(null);

  private _isInit: boolean = false;
  private _isLoading: boolean;

  private _client: AxiosInstance;

  get subject(): BehaviorSubject<Results> {
    if (!this._isInit) {
      this.init();
    }
    return this._$subject;
  }

  get observable(): Observable<Results> {
    if (!this._isInit) {
      this.init();
    }
    return this._$subject.asObservable();
  }

  get isLoading() {
    return this._isLoading;
  }

  constructor(
    protected request: APIRequest,
    private loadingService: NetworkLoadingService,
    protected socketService: SocketService,
    protected args?: Params
  ) {
    if (this.request) {
      if (this.request.target === APIS.auth) {
        var baseURL = environment.APIS_BASE_URL_AUTH;
      } else {
        var baseURL = environment.APIS_BASE_URL_BACK;
      }

      this._client = axios.create({
        baseURL,
        headers: {
          'Content-Type': 'application/json',
          Authorization: ''
        }
      });

      this._client.interceptors.request.use(
        async function (config) {
          try {
            const jwt = session.getTokenAsCookie();
            if (jwt) config.headers.Authorization = 'Bearer ' + jwt;
          } catch (err) {
            return config;
          }
          return config;
        },
        function (err) {
          return Promise.reject(err);
        }
      );
    }
  }

  protected init() {
    // console.log('init()');
    if (this.request) {
      // const observable = new Observable(subscriber => {
      // this._$subject.next(null);
      let promise: Promise<AxiosResponse>;

      if (this.request.method === APISMethods.post) {
        promise = this._client.post(this.request.url, this.args);
      } else if (this.request.method === APISMethods.get) {
        promise = this._client.get(this.request.url);
      } else if (this.request.method === APISMethods.put) {
        promise = this._client.put(this.request.url, this.args);
      } else if (this.request.method === APISMethods.delete) {
        promise = this._client.delete(this.request.url, this.args);
      }

      promise
        .then(res => {
          this._$subject.next(res.data);
          // subscriber.next(res);
        })
        .catch(err => {
          // this._$subject.next(err.response.data);
          // subscriber.next(err.response.data.error);
          // throw err.response.data;
          throw err.response.data.error;
        })
        .finally(() => {
          this._isLoading = false;
          if (this.loadingService) this.loadingService.unsubscribe();
        });
      // });

      // this._$subject = observable.pipe(
      //   distinctUntilChanged(),
      //   shareReplay({ bufferSize: 1, refCount: true })
      // );

      this._isLoading = true;
      if (this.loadingService) this.loadingService.subscribe();

      // function joinAGame(slug) {
      // 	return _client.post(`/games/join`, {slug: slug})
      // }

      // // function getCurrent() {
      // // 	// return _client.get(`/games/current`)
      // // 	return _client.get(`/game`)
      // // }

      // ----------------------------

      // this._loading$ = observable.pipe(
      //   map(res => res.loading),
      //   distinctUntilChanged(isEqual),
      //   shareReplay({ bufferSize: 1, refCount: true })
      // );

      // this._$subject = observable.pipe(
      //   filter(res => !res.loading),
      //   map(res => res.data),
      //   distinctUntilChanged(isEqual),
      //   scan<QueryResult, Results>((acc, value, index) => {
      //     return this.map(value, acc, index);
      //   }, null),
      //   distinctUntilChanged(),
      //   shareReplay({ bufferSize: 1, refCount: true })
      // );
    }
    this._isInit = true;
  }

  public refetch(args?: Params) {
    if (args) {
      this.args = args;
    }
    console.log('refetch()');
    this.init();
  }

  protected reset() {}

  protected mutate<ReturnType, ArgsType>(request: APIRequest, args?: ArgsType): Promise<ReturnType> {
    return mutate<ReturnType>(request, args, this.loadingService, this._client);
  }

  // protected registerSocketEvent(event: string, callback: (...args) => void) {
  //   this.socketService.socket.on(event, callback);
  // }

  protected emitSocketEvent<T>(socketRequest: SocketRequest<T>) {
    this.socketService.emit(socketRequest);
  }

  protected abstract map(res: Results): Results;
}
