import { AxiosInstance, AxiosResponse } from 'axios';
import { APISMethods } from '../../../../../../models/enums';
import { APIRequest } from '../../../../../../models/interfaces';
import { NetworkLoadingService } from '../network.loading/network.loading.service';

export function mutate<ReturnType>(
  request: APIRequest,
  args: any,
  loadingService: NetworkLoadingService,
  client: AxiosInstance
): Promise<ReturnType> {
  loadingService.subscribe();

  let promise: Promise<AxiosResponse>;

  if (request.method === APISMethods.post) {
    promise = client.post(request.url, args);
  } else if (request.method === APISMethods.get) {
    promise = client.get(request.url);
  } else if (request.method === APISMethods.put) {
    promise = client.put(request.url, args);
  } else if (request.method === APISMethods.delete) {
    promise = client.delete(request.url, args);
  }

  return promise
    .then(res => {
      return res.data;
      // return res;
    })
    .catch(err => {
      // return err.response.data;
      // return err.response.data.error;
      // throw err.response.data;
      throw err.response.data.error;
    })
    .finally(() => {
      loadingService.unsubscribe();
    });
}
