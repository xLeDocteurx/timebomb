import { Injectable } from '@angular/core';
import { Dic } from '../../../../../../models/interfaces';
import { Sound, SoundsLibrary } from './types/sound.interface';

@Injectable({
  providedIn: 'root'
})
export class SoundService {
  private soundsLibrary: Dic<Sound> = SoundsLibrary;
  private audioElements: Dic<HTMLAudioElement> = {};

  constructor() {}

  async playSound(
    name: string,
    loop?: boolean,
    volume?: number
  ): Promise<() => void> {
    const loopValue = loop !== undefined ? loop : this.soundsLibrary[name].loop;
    const volumeValue =
      volume !== undefined ? volume : this.soundsLibrary[name].volume;

    await this.loadSound(name, loopValue, volumeValue);
    this.stopSound(name);
    this.audioElements[name].play();
    return () => this.stopSound(name);
  }

  private stopSound(name: string): void {
    this.audioElements[name].pause();
    this.audioElements[name].currentTime = 0;
  }

  private async loadSound(
    name: string,
    loop: boolean,
    volume: number
  ): Promise<void> {
    const sound: Sound = this.soundsLibrary[name];
    if (!this.audioElements[name]) {
      this.audioElements[name] = await this.handleSoundLoading(sound.path);
      this.audioElements[name].loop = loop;
      this.audioElements[name].volume = volume;
    }
  }

  private async handleSoundLoading(path: string): Promise<HTMLAudioElement> {
    await new Promise((resolve, reject) => {
      const sound = new Audio(path);
      sound.oncanplaythrough = () => {
        resolve(sound);
      };
    });
    return new Promise((resolve, reject) => {
      const sound = new Audio(path);
      sound.oncanplaythrough = () => {
        resolve(sound);
      };
    });
  }
}
