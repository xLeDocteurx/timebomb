import { Dic } from '../../../../../../../models/interfaces/index';

export interface Sound {
  path: string;
  loop: boolean;
  volume: number;
}

export const SoundsLibrary: Dic<Sound> = {
  // call_ring: {
  //   path: '/assets/sounds/call/ring.mp3',
  //   loop: true,
  //   volume: 1
  // }
};
