module.exports = {
  "/auth": {
    onProxyReq: (proxyRes, req, res) => {
      proxyRes.setHeader('X-Real-ip', req.connection.remoteAddress)
    },
    "target": "http://localhost:1339",
    "secure": false,
    "pathRewrite": {
      "^/auth": ""
    },
    "ws": true
  },
  "/api": {
    onProxyReq: (proxyRes, req, res) => {
      proxyRes.setHeader('X-Real-ip', req.connection.remoteAddress)
    },
    "target": "http://localhost:1337",
    "secure": false,
    "pathRewrite": {
      "^/api": ""
    }
  },
  "/socket": {
    onProxyReq: (proxyRes, req, res) => {
      proxyRes.setHeader('X-Real-ip', req.connection.remoteAddress)
    },
    "target": "http://localhost:1338",
    "secure": false,
    "pathRewrite": {
      "^/socket": ""
    }
  }
}
