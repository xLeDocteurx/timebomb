import * as dotenv from 'dotenv';
import { Game } from '../../../models/interfaces';
import { Mongo } from '../utils';

dotenv.config({
  path: `../../config/${
    process.env.NODE_ENV ? process.env.NODE_ENV : 'local'
  }.env`
  // debug: true
});

export const GamesCollection = Mongo.getCollection<Game>(
  process.env.DATABASE,
  'games'
);
