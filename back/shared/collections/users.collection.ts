import * as dotenv from 'dotenv';
import { User } from '../../../models/interfaces';
import { Mongo } from '../utils';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

export const UsersCollection = Mongo.getCollection<User>(process.env.DATABASE, 'users');
