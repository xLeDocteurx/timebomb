// import { users } from '@controllers';
// import { decryptToken } from '@utils';
import { users } from '../controllers/index';
import { decryptToken } from '../utils/index';

export function checkPermissions(err, req, res, next) {
  const { authorization } = req.headers;
  if (!authorization || authorization == '') {
    res.status(401).json({ error: `No authorization in header's request` });
  }

  const token = authorization.split('Bearer ')[1];
  if (!token) {
    res.status(401).json({ error: `No token in the authorization header` });
  }

  const userId = decryptToken(token);
  if (!userId) {
    res.status(401).json({ error: `Can't decrypt the token as a valid userId` });
  }

  if (!users.exist(userId)) {
    res.status(401).json({ error: `Can't find the user associated with the token` });
  }

  next();
}
