import * as dotenv from 'dotenv';
import { Challenge, PasswordSalt, PrivateKeySalt } from '../../../models/interfaces';
import { generateString } from '../../../utils';
import { HashAlgType } from '../../../utils/crypto/enums';
import { Redis, RedisDatabases } from '../utils';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

export async function generatePasswordSalt(): Promise<PasswordSalt> {
  const redisId = generateString(process.env.REDIS_KEY_LENGTH);
  const value = generateString(process.env.SALT_STR_LENGTH);
  await Redis.set(RedisDatabases.passwordSalts, redisId, value);

  return {
    redisId,
    value,
    iterations: Number(process.env.HSEC_ITERATIONS),
    keyAlgs: {
      hash: HashAlgType.SHA256,
      hmac: 'sha256'
    }
  };
}

export async function generatePrivateKeySalt(): Promise<PrivateKeySalt> {
  const redisId = generateString(process.env.REDIS_KEY_LENGTH);
  const value = generateString(process.env.SALT_STR_LENGTH);
  await Redis.set(RedisDatabases.privateKeySalts, redisId, value);

  return {
    redisId,
    value,
    iterations: Number(process.env.HSEC_ITERATIONS)
  };
}

export async function generateChallenge(): Promise<Challenge> {
  const redisId = generateString(process.env.REDIS_KEY_LENGTH);
  const value = generateString(process.env.CHALLENGE_STR_LENGTH);
  await Redis.set(RedisDatabases.challenges, redisId, value);

  return {
    redisId,
    value
    // salt: generateString(process.env.SALT_STR_LENGTH),
    // iterations: Number(process.env.HSEC_ITERATIONS),
    // keyAlgs: {
    //   hash: HashAlgType.SHA256,
    //   hmac: 'sha256'
    // }
  };
}
