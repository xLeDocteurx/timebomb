import * as dotenv from 'dotenv';
import * as http from 'http';
import { ObjectId, UpdateWriteOpResult } from 'mongodb';
import slugify from 'slugify';
import { CardsTypes, GameStatus, GameTypes } from '../../../models/enums/index';
import { Dic, Game, GameInput, GameState, GameStatePlayer, PublicGame } from '../../../models/interfaces';
import { clone, generateString, randomInt } from '../../../utils';
import { GamesCollection, UsersCollection } from '../collections';
import { decryptToken, filterGame, gameEngine, populateGame } from '../utils/';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

export async function getCurrent(reqHeaders: http.IncomingHttpHeaders): Promise<PublicGame>;
export async function getCurrent(token: string): Promise<PublicGame>;
export async function getCurrent(input): Promise<PublicGame> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var userId = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const user = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(userId)
  });
  if (!user) {
    throw `You do not exist`;
  }

  const game = await (
    await GamesCollection
  ).findOne({
    _id: user.currentGame
  });
  if (!game) {
    throw `This game does not exist`;
  }

  if (!game.players.some(playerObjId => playerObjId.toHexString() === userId)) {
    throw 'You are not in this game';
  }

  const populatedGame = await populateGame(game);
  if (
    game.status === GameStatus.inProgress &&
    game.players.some(playerObjId => {
      return playerObjId.toHexString() === userId;
    })
  ) {
    populatedGame.userTeam = game.state.players[userId].team;
    populatedGame.userNumerofDiffuse = game.state.players[userId].numerOfDiffuse;
    populatedGame.userHasTheBomb = game.state.players[userId].hasTheBomb;
  }

  return filterGame(populatedGame);
}

export async function findAllAvailables() {
  const games: Game[] = await (await GamesCollection).find({ status: GameStatus.open }).toArray();

  // if (games.length < 1) {
  //   throw 'No game found';
  // }

  const publicGames: PublicGame[] = await Promise.all(
    games.map(async game => {
      return await populateGame(game);
    })
  );

  const filteredGames = publicGames.map(game => {
    return filterGame(game);
  });

  return filteredGames;
}

export async function findOneBySlug(reqHeaders: http.IncomingHttpHeaders, slug: string): Promise<PublicGame>;
export async function findOneBySlug(token: string, slug: string): Promise<PublicGame>;
export async function findOneBySlug(input, slug: string): Promise<PublicGame> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var userId = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const user = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(userId)
  });
  if (!user) {
    throw `You do not exist`;
  }

  const game = await (await GamesCollection).findOne({ slug });

  if (!game) {
    throw 'Game not found';
  }

  const populatedGame = await populateGame(game);

  if (
    game.status === GameStatus.inProgress &&
    game.players.some(playerObjId => {
      return playerObjId.toHexString() === userId;
    })
  ) {
    if (!game.players.some(playerObjId => playerObjId.toHexString() === userId)) {
      throw 'You are not in this game';
    }
    populatedGame.userTeam = game.state.players[userId].team;
    populatedGame.userNumerofDiffuse = game.state.players[userId].numerOfDiffuse;
    populatedGame.userHasTheBomb = game.state.players[userId].hasTheBomb;
  }

  // console.log('populatedGame', populatedGame);

  return filterGame(populatedGame);
}

// async function exist(_id) {

//   await dbConnect()

//   const user = await (await GamesCollection).findOne({_id})

//   await dbDisconnect()
//   if(!user) {
//     return false
//   } else {
//     return true
//   }

// }

// TODO : gestion d'erreur sur ce controlleur
export async function create(gameInput: GameInput) {
  const options = gameInput.options;
  const creatorId = decryptToken(gameInput.authorization.split('Bearer ')[1]);

  const player = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(creatorId)
  });
  if (!player) {
    throw 'You do not exist';
  }

  if (player.currentGame) {
    throw 'You are already in a game';
  }

  const slug = `${slugify(options.name, {
    replacement: '_', // replace spaces with replacement
    remove: /[*+~.()'"!:@]/g, // regex to remove charactersv
    lower: true // result in lower case
  })}_${generateString(5, 'hexa')}`;

  if (
    await (
      await GamesCollection
    ).findOne({
      slug: slug
    })
  ) {
    throw 'A game with same slug already exist';
  }

  const game: Game = {
    name: options.name,
    slug: slug,
    type: GameTypes.correspondence,

    status: GameStatus.open,

    creator: new ObjectId(creatorId),
    players: [new ObjectId(creatorId)],

    created: new Date(),
    started: null,
    ended: null
  };

  const insertedGame = await (await GamesCollection).insertOne(game);

  const creator = await (
    await UsersCollection
  ).updateOne(
    {
      _id: new ObjectId(creatorId)
    },
    { $set: { currentGame: insertedGame.insertedId } }
  );

  return true;
}

export async function join(reqHeaders: http.IncomingHttpHeaders, slug: string): Promise<boolean>;
export async function join(token: string, slug: string): Promise<boolean>;
export async function join(input, slug: string): Promise<boolean> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var userId = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const player = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(userId)
  });
  if (!player) {
    throw 'You do not exist';
  }

  const game = await (await GamesCollection).findOne({ slug });

  if (!game) {
    throw 'No game found';
  }

  if (player.currentGame) {
    throw 'You are already in a game';
  }

  if (game.players.some(playerObjId => playerObjId.toHexString() === userId)) {
    throw 'You are already in the game';
  }

  if (game.status !== GameStatus.open) {
    throw 'The game is not open';
  }

  if (game.players.length >= (process.env.MAX_PLAYERS as unknown as number)) {
    throw `The game is full`;
  }

  await (await GamesCollection).updateOne({ _id: game._id }, { $addToSet: { players: player._id } });

  await (await UsersCollection).updateOne({ _id: player._id }, { $set: { currentGame: game._id } });

  return true;
}

export async function leave(reqHeaders: http.IncomingHttpHeaders, slug: string): Promise<boolean>;
export async function leave(token: string, slug: string): Promise<boolean>;
export async function leave(input, slug: string): Promise<boolean> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var userId = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const player = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(userId)
  });
  if (!player) {
    throw 'You do not exist';
  }

  const game = await (await GamesCollection).findOne({ slug });

  if (!game) {
    throw 'No game found';
  }

  if (!player.currentGame) {
    throw 'You are not in a game';
  }

  if (!game.players.some(playerObjId => playerObjId.toHexString() === userId)) {
    throw 'You are not in the game';
  }

  if (game.status !== GameStatus.open) {
    throw 'The game is not open';
  }

  await (await GamesCollection).updateOne({ _id: game._id }, { $pull: { players: player._id } });

  await (await UsersCollection).updateOne({ _id: player._id }, { $set: { currentGame: null } });

  return true;
}

export async function launch(reqHeaders: http.IncomingHttpHeaders, slug: string): Promise<UpdateWriteOpResult>;
export async function launch(token: string, slug: string): Promise<UpdateWriteOpResult>;
export async function launch(input, slug: string): Promise<UpdateWriteOpResult> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var userId = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const user = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(userId)
  });
  if (!user) {
    throw 'You do not exist';
  }

  const game = await (await GamesCollection).findOne({ slug });

  if (!game) {
    throw 'No game found';
  }

  // // Tester ce cas d'erreur pour l'orsque le joueur qui veut rejojndre y est déja
  // if (game.user1 == userId || game.user2 == userId)
  //   throw 'You already are in the game';
  // // Tester ce cas d'erreur pour l'orsque la partie est complète
  // if (game.user2) throw 'This game is full';

  if (!game.players.some(playerObjId => playerObjId.toHexString() === userId)) {
    throw 'You are not in this game';
  }

  if (game.creator.toHexString() !== userId) {
    throw 'You are not the creator of the game';
  }

  // if (game.status === GameStatus.inProgress) {
  //   throw 'Game has already started';
  // }

  if (game.status !== GameStatus.open) {
    throw 'Game is not open';
  }
  if (game.players.length < (process.env.MIN_PLAYERS as unknown as number)) {
    throw `You need at least ${process.env.MIN_PLAYERS} players to launch a game`;
  }

  const teamArray = gameEngine.generateTeamArray(game.players.length);
  // TODO : enlever la condition et mettre 0
  const handsArray: CardsTypes[][] = gameEngine.generateHandsArray(game.players.length, 0, 0);

  const gameStatePlayers: Dic<GameStatePlayer> = {};
  game.players.forEach((player, player_index) => {
    const hand = handsArray[player_index];
    gameStatePlayers[player.toHexString()] = {
      id: player,
      team: teamArray[player_index],
      numberOfCards: 5,
      numerOfDiffuse: hand.filter(card => card === CardsTypes.diffuse).length,
      hasTheBomb: hand.some(card => card === CardsTypes.bomb)
    };
  });

  const res = await (
    await GamesCollection
  ).updateOne(
    { _id: game._id },
    {
      $set: {
        status: GameStatus.inProgress,
        started: new Date(),
        state: {
          turn: 0,
          playerToPlay: game.players[Math.round(randomInt() * (game.players.length - 1))],
          players: gameStatePlayers,
          cardsOut: 0,
          diffuseOut: 0
        }
      }
    }
  );

  return res;
}

export async function revealCard(reqHeaders: http.IncomingHttpHeaders, playerId: string): Promise<CardsTypes>;
export async function revealCard(token: string, playerId: string): Promise<CardsTypes>;
export async function revealCard(input, playerId: string): Promise<CardsTypes> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var userId = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const user = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(userId)
  });
  if (!user) {
    throw 'You do not exist';
  }

  const player = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(playerId)
  });
  if (!player) {
    throw 'This player do not exist';
  }

  const game = await (
    await GamesCollection
  ).findOne({
    _id: user.currentGame
  });
  if (!game) {
    throw 'Game not found';
  }

  if (!game.players.some(playerObjId => playerObjId.toHexString() === userId)) {
    throw 'You are not in this game';
  }

  if (!game.players.some(playerObjId => playerObjId.toHexString() === playerId)) {
    throw 'The other player is not in this game';
  }

  if (game.state.playerToPlay.toHexString() !== userId) {
    throw 'You are not allowed to play now';
  }

  if (game.state.players[playerId].numberOfCards < 1) {
    throw 'This player does not have any card to reveal';
  }

  // TODO : si turn égale à 3 throw

  const newGameState: GameState = clone(game.state);

  const revealedCard = gameEngine.revealCard(newGameState.players[playerId]);

  newGameState.cardsOut += 1;
  newGameState.players[playerId].numberOfCards -= 1;
  if (revealedCard === CardsTypes.bomb) {
    // Victoire red team : bombe explose
    console.log('bombe explose');
    endTheGame(game);
  } else if (revealedCard === CardsTypes.diffuse) {
    newGameState.diffuseOut += 1;
    newGameState.players[playerId].numerOfDiffuse -= 1;
  }

  if (newGameState.diffuseOut === game.players.length) {
    // Victoire blue team : bombe désamorcée
    console.log('bombe désamorcée');
    endTheGame(game);
  }

  if (newGameState.cardsOut === game.players.length * 5 - 5) {
    // Victoire red team : bombe non désamorcée
    console.log('bombe non désamorcée');
    endTheGame(game);
  }
  newGameState.playerToPlay = new ObjectId(playerId);

  // Si erreur renvoyer l'object d'origine surchargé d'un message d'erreur
  // plutot que de faire un throw qui sera difficilement récupérable par le socket ?

  // Si tour suivant
  if (newGameState.cardsOut === (newGameState.turn + 1) * game.players.length) {
    newGameState.turn += 1;
    newGameState.newTurn = true;
    // const teamArray = gameEngine.generateTeamArray(game.players.length);
    const handsArray: CardsTypes[][] = gameEngine.generateHandsArray(
      game.players.length,
      newGameState.diffuseOut,
      newGameState.turn
    );

    game.players.forEach((player, player_index) => {
      const hand = handsArray[player_index];
      newGameState.players[player.toHexString()] = {
        id: player,
        team: game.state.players[player.toHexString()].team,
        numberOfCards: hand.length,
        numerOfDiffuse: hand.filter(card => card === CardsTypes.diffuse).length,
        hasTheBomb: hand.some(card => card === CardsTypes.bomb)
      };
    });

    // TODO : A dégager si algo assé solide
    if (newGameState.turn >= 4) {
      throw 'WTF Nombre de tours max dépassé';
    }
  }
  // newGameState.newTurn = false;

  // console.log(
  //   '(await (await GamesCollection).findOne({_id: game._id})).state',
  //   (await (await GamesCollection).findOne({ _id: game._id })).state
  // );

  await (await GamesCollection).updateOne({ _id: game._id }, { $set: { state: newGameState } });
  // console.log(
  //   '(await (await GamesCollection).findOne({_id: game._id})).state',
  //   (await (await GamesCollection).findOne({ _id: game._id })).state
  // );

  return revealedCard;
}

export async function endTheGame(game: Game) {
  await (await GamesCollection).updateOne({ _id: game._id }, { $set: { status: GameStatus.ended } });

  // await (await UsersCollection).updateMany(
  //   { _id: { $in: game.players } },
  //   { $set: { currentGame: null }, $inc: { wins: 1 } }
  // );
  await (
    await UsersCollection
  ).updateMany({ _id: { $in: game.players } }, { $set: { currentGame: null }, $inc: { games: 1 } });

  // set un champ sur la game pour indiquer quelle team a gagnée

  throw 'This is the end';
  return true;
}
