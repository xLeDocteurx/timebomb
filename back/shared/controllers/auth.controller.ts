import * as dotenv from 'dotenv';
import * as md5 from 'js-md5';
import { AvatarTypes, Roles } from '../../../models/enums';
import {
  ConnectResponse,
  GetPasswordAndPrivateKeySaltsResponse,
  GetPasswordSaltsAndChallengeInput,
  GetPasswordSaltsAndChallengeResponse,
  PasswordSalt,
  PrivateKeySalt,
  RegisterResponse,
  User,
  UserInputConnect,
  UserInputRegister
} from '../../../models/interfaces';
import { h, HashAlgType, hmac, xor } from '../../../utils';
import { filterPrivateUser, generateToken, Mongo, Redis, RedisDatabases } from '../utils/index';
import { generateChallenge, generatePasswordSalt, generatePrivateKeySalt } from './crypto.controller';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

const DATABASE = process.env.DATABASE;

export async function getPasswordAndPrivateKeySalts(): Promise<GetPasswordAndPrivateKeySaltsResponse> {
  return {
    passwordSalt: await generatePasswordSalt(),
    privateKeySalt: await generatePrivateKeySalt()
  };
}

export async function register(userInput: UserInputRegister): Promise<RegisterResponse> {
  if (
    await (
      await Mongo.getCollection(DATABASE, 'users')
    ).findOne({
      username: userInput.username
    })
  ) {
    throw 'An user with same username already exist';
  }

  if (
    await (
      await Mongo.getCollection(DATABASE, 'users')
    ).findOne({
      email: userInput.email
    })
  ) {
    throw 'An user with same email already exist';
  }

  const passwordSaltValue = await Redis.getAndDelete(RedisDatabases.passwordSalts, userInput.passwordSaltId);

  if (!passwordSaltValue) {
    throw 'This password salt does not exist';
  }

  const passwordSalt: PasswordSalt = {
    value: passwordSaltValue,
    iterations: Number(process.env.HSEC_ITERATIONS),
    keyAlgs: {
      hash: HashAlgType.SHA256,
      hmac: 'sha256'
    }
  };

  const privateKeySaltValue = await Redis.getAndDelete(RedisDatabases.privateKeySalts, userInput.privateKeySaltId);

  if (!privateKeySaltValue) {
    throw 'This private key salt does not exist';
  }

  const privateKeySalt: PrivateKeySalt = {
    value: privateKeySaltValue,
    iterations: Number(process.env.HSEC_ITERATIONS)
  };

  const newUser: User = {
    username: userInput.username,
    email: userInput.email,

    gravatarHash: md5(userInput.email.toLowerCase()),
    avatarType: userInput.avatarType as AvatarTypes,

    auth: {
      password: {
        hash: userInput.passwordHash,
        salt: passwordSalt
      },
      keys: {
        publics: {
          current: userInput.publicKey
        },
        encryptedPrivate: {
          value: userInput.encryptedPrivate,
          salt: privateKeySalt
        }
      }
    },

    role: Roles.registered,
    confirmed: false,
    currentGame: null,
    friends: [],
    games: 0,
    wins: 0
  };

  const insertedUser = await (await Mongo.getCollection<User>(DATABASE, 'users')).insertOne(newUser);

  const filteredUser = filterPrivateUser(newUser);
  const token = generateToken(newUser._id);

  return { token };
}

export async function getPasswordSaltsAndChallenge(
  userInput: GetPasswordSaltsAndChallengeInput
): Promise<GetPasswordSaltsAndChallengeResponse> {
  const user = await (await Mongo.getCollection<User>(DATABASE, 'users')).findOne({ email: userInput.email });

  if (!user) throw `This user does not exist`;

  return {
    passwordSalt: user.auth.password.salt,
    challenge: await generateChallenge()
  };
}

export async function connect(userInputLight: UserInputConnect): Promise<ConnectResponse> {
  const user = await (await Mongo.getCollection<User>(DATABASE, 'users')).findOne({ email: userInputLight.email });

  if (!user) throw `This user does not exist`;

  const challenge = Redis.getAndDelete(RedisDatabases.challenges, userInputLight.passwordChallengeId);

  if (!challenge) throw `This challenge does not exist`;

  if (
    h(
      xor(
        userInputLight.passwordHash,
        hmac(
          h(user.auth.password.hash, user.auth.password.salt.keyAlgs.hash),
          user.auth.password.salt.value,
          user.auth.password.salt.keyAlgs.hmac
        )
      ) === h(user.auth.password)
    )
  ) {
    const filteredUser = filterPrivateUser(user);
    const token = generateToken(user._id);

    return {
      token,
      keys: {
        public: user.auth.keys.publics.current,
        encryptedPrivate: {
          value: user.auth.keys.encryptedPrivate.value,
          salt: user.auth.keys.encryptedPrivate.salt
        }
      }
    };
  } else {
    throw `Wrong password`;
  }
}
