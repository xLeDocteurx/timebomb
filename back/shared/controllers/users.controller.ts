import * as dotenv from 'dotenv';
import * as http from 'http';
import { ObjectId } from 'mongodb';
import { PrivateUser, PublicUser } from '../../../models/interfaces';
import { UsersCollection } from '../collections';
import { decryptToken, filterPrivateUser, filterPublicUser, populateUser } from '../utils/index';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

export async function getCurrent(reqHeaders: http.IncomingHttpHeaders): Promise<PrivateUser>;
export async function getCurrent(token: string): Promise<PrivateUser>;
export async function getCurrent(input): Promise<PrivateUser> {
  let token: string;
  if (typeof input === 'object') {
    const { authorization } = input;
    token = authorization.split('Bearer ')[1];
  } else {
    token = input;
  }
  try {
    var _id = decryptToken(token);
  } catch (err) {
    throw err;
  }

  const user = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(_id)
  });
  if (!user) {
    throw `You do not exist`;
  }

  const populatedUser = await populateUser<PrivateUser>(user);
  return filterPrivateUser(populatedUser);
}

export async function findAll() {
  // await Mongo.connect();
  const users = await (await UsersCollection).find().toArray();
  if (users.length < 1) {
    throw 'No user';
  }
  const populatedUsers = await Promise.all(
    users.map(async user => {
      return await populateUser<PublicUser>(user);
    })
  );
  const filteredUsers = populatedUsers.map(user => {
    return filterPublicUser(user);
  });

  return filteredUsers;
  // .exec((err, users) => {
  //   console.log(users)
  // })
}

export async function findOneByUsername(username) {
  // await Mongo.connect();

  const user = await (await UsersCollection).findOne({ username });

  if (!user) {
    // await Mongo.close();
    throw 'User not found';
  }

  const populatedUser = await populateUser<PublicUser>(user);

  // await Mongo.close();
  return filterPublicUser(populatedUser);
  // .exec((err, user) => {
  //   console.log(user)
  // })
}

export async function exist(_id: string) {
  // await Mongo.connect();

  const user = await (
    await UsersCollection
  ).findOne({
    _id: new ObjectId(_id)
  });

  // await Mongo.close();

  if (!user) {
    return false;
  } else {
    return true;
  }
}
