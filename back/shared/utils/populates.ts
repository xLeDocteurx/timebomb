import { Game, PublicGame, User } from '../../../models/interfaces';
import { clone } from '../../../utils/hidash';
import { GamesCollection, UsersCollection } from '../collections';
import { filterGame, filterPublicUser } from './filters';

export async function populateGame(game: Game): Promise<PublicGame> {
  const gameAsAny: any = clone(game);
  gameAsAny.creator = filterPublicUser(
    await (
      await UsersCollection
    ).findOne({
      _id: game.creator
    })
  );
  gameAsAny.players = (
    await (
      await UsersCollection
    )
      .find({
        _id: {
          $in: game.players
        }
      })
      .toArray()
  ).map(user => filterPublicUser(user));
  return gameAsAny as PublicGame;
}

export async function populateUser<T>(user: User): Promise<T> {
  const userAsAny: any = clone(user);
  if (user.currentGame) {
    userAsAny.currentGame = filterGame(
      await (
        await GamesCollection
      ).findOne({
        _id: user.currentGame
      })
    );
  }
  return userAsAny as T;
}
