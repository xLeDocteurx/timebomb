import {
  Game,
  GameState,
  GameStatePlayer,
  PrivateUser,
  PublicGame,
  PublicGameState,
  PublicGameStatePlayer,
  PublicUser,
  User
} from '../../../models/interfaces';
import { clone, omit } from '../../../utils';

export function filterPrivateUser(user: User | PrivateUser): PrivateUser {
  let filteredUser = omit(user, [
    // '_id',
    '__v',
    'password',
    'socketId'
  ]) as any;

  return filteredUser as PrivateUser;
}

export function filterPublicUser(user: User | PublicUser): PublicUser {
  let filteredUser = omit(user, [
    // '_id',
    '__v',
    'email',
    'password',
    'confirmed',
    'friends',
    'socketId'
  ]) as any;

  // if (filteredUser.role == 0) {
  //   filteredUser.role = 'God';
  // } else if (filteredUser.role == 1) {
  //   filteredUser.role = 'Admin';
  // } else if (filteredUser.role == 2) {
  //   filteredUser.role = 'Registered user';
  // } else {
  //   filteredUser.role = 'WTF';
  // }

  return filteredUser as PublicUser;
}

export function filterGame(game: Game | PublicGame): PublicGame {
  let filteredGame = omit(game, ['_id', '__v']) as any;
  if (filteredGame.state) {
    filteredGame.state = filterGameState(game.state);
  }
  return filteredGame as PublicGame;
}

export function filterGameState(gameState: GameState | PublicGameState, userId?: string): PublicGameState {
  const gameStateAsAny: any = clone(gameState);
  gameStateAsAny.players = {};
  Object.keys(gameState.players).forEach(gameStatePlayerKey => {
    gameStateAsAny.players[gameStatePlayerKey] = filterGameStatePlayer(gameState.players[gameStatePlayerKey]);
  });
  // let filteredGameState = omit(gameState, ['blbl']);
  return gameStateAsAny as PublicGameState;
}

export function filterGameStatePlayer(gameStatePlayer: GameStatePlayer | PublicGameStatePlayer): PublicGameStatePlayer {
  let filteredGameStatePlayer = omit(gameStatePlayer, ['team', 'numerOfDiffuse', 'hasTheBomb']) as any;
  return filteredGameStatePlayer as PublicGameStatePlayer;
}
