import * as dotenv from 'dotenv';
import { Db, MongoClient } from 'mongodb';
import { Dic } from '../../../models/interfaces';

dotenv.config({
  path: `../../config/${
    process.env.NODE_ENV ? process.env.NODE_ENV : 'local'
  }.env`
  // debug: true
});

export class Mongo {
  private static instance: MongoClient;
  private static dbs: Dic<Db> = {};
  // private static db: Db;

  constructor() {}

  static async connect() {
    if (this.instance) {
      return this.instance;
    } else {
      // console.log('pocess.env.DATABASE_URL', process.env.DATABASE_URL);
      this.instance = await MongoClient.connect(process.env.DATABASE_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      });

      return this.instance;
    }
  }

  static async close() {
    if (this.instance) {
      await this.instance.close();
      delete this.instance;
    }
  }

  static async getDatabase(database: string) {
    if (!this.dbs[database]) {
      await this.connect();
      this.dbs[database] = this.instance.db(database);
      // this.db = this.instance.db(database);
    }
    return this.dbs[database];
  }

  static async getCollection<T>(database: string, collection: string) {
    return (await this.getDatabase(database)).collection<T>(collection);
  }
}
