import { CardsTypes, PlayerTeams } from '../../../models/enums';
import { GameStatePlayer } from '../../../models/interfaces';
import { randomInt, shuffle } from '../../../utils';

export function generateTeamArray(numberOfPlayers: number) {
  let teamsArray = [];

  let numberOfVilains: number;
  if (numberOfPlayers === 4) {
    numberOfVilains = 1;
  } else if (numberOfPlayers === 5) {
    numberOfVilains = Math.floor(randomInt(1, 2));
  } else if (numberOfPlayers === 6) {
    numberOfVilains = 2;
  } else if (numberOfPlayers === 7) {
    numberOfVilains = Math.floor(randomInt(2, 3));
  } else if (numberOfPlayers === 8) {
    numberOfVilains = 3;
  } else {
    numberOfVilains = Math.floor(numberOfPlayers * 0.375);
  }

  for (let i = 0; i < numberOfPlayers - numberOfVilains; i++) {
    teamsArray.push(PlayerTeams.blue);
  }
  for (let i = 0; i < numberOfVilains; i++) {
    // teamsArray.splice(
    //   Math.round(randomInt() * (teamsArray.length - 1)),
    //   0,
    //   PlayerTeams.red
    // );
    teamsArray.push(PlayerTeams.red);
  }

  console.log('teamsArray before shuffle : ', teamsArray);
  teamsArray = shuffle(teamsArray);
  console.log('teamsArray after shuffle : ', teamsArray);

  return teamsArray;
}

export function generateHandsArray(numberOfPlayers: number, diffuseOut: number, turn: number): CardsTypes[][] {
  let deckArray = [];
  const totalNumberOfCards = numberOfPlayers * (5 - turn);
  const numberOfDiffuseLeft = numberOfPlayers - diffuseOut;

  for (let i = 0; i < totalNumberOfCards - numberOfDiffuseLeft - 1; i++) {
    deckArray.push(CardsTypes.standard);
  }
  for (let i = 0; i < numberOfDiffuseLeft; i++) {
    deckArray.push(CardsTypes.diffuse);
  }
  deckArray.push(CardsTypes.bomb);

  deckArray = shuffle(deckArray);

  const handsArray = [];
  for (let i = 0; i < numberOfPlayers; i++) {
    const handArray = [];
    for (let j = i * (5 - turn); j < (i + 1) * (5 - turn); j++) {
      handArray.push(deckArray[j]);
    }
    handsArray.push(handArray);
  }

  return handsArray;
}

export function generateHandArray(player: GameStatePlayer): CardsTypes[] {
  const handArray: CardsTypes[] = [];

  for (let i = 0; i < player.numberOfCards - player.numerOfDiffuse - (player.hasTheBomb ? 1 : 0); i++) {
    handArray.push(CardsTypes.standard);
  }
  if (player.numerOfDiffuse) {
    for (let i = 0; i < player.numerOfDiffuse; i++) {
      handArray.push(CardsTypes.diffuse);
    }
  }
  if (player.hasTheBomb) {
    handArray.push(CardsTypes.bomb);
  }

  return handArray;
}

export function revealCard(player: GameStatePlayer): CardsTypes {
  let handArray = generateHandArray(player);
  handArray = shuffle(handArray);
  return handArray[randomInt(handArray.length - 1)];
}

export function countVisibleTokens(game) {
  // remplir un plateau
  let board = [];
  for (let y = 0; y < game.tokensPerPlayer; y++) {
    board.push([]);
    for (let x = 0; x < game.tokensPerPlayer; x++) {
      const cell = { x: x, y: y, tokens: [] };
      board[y].push(cell);
    }
  }
  game.moves.forEach((move, moveIndex) => {
    board[move.cell.y][move.cell.x].tokens.push(move);
  });
  // compter les tokens visibles
  let blueTokens = 0;
  let redTokens = 0;

  board.forEach(col => {
    col.forEach(row => {
      if (row.tokens.length > 0) {
        if (row.tokens[row.tokens.length - 1].player == 'bluePlayer') {
          blueTokens += 1;
        } else {
          redTokens += 1;
        }
      }
    });
  });
  return {
    blue: blueTokens,
    red: redTokens
  };
}
