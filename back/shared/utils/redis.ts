import * as dotenv from 'dotenv';
import * as redis from 'redis';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

export enum RedisDatabases {
  passwordSalts = 'PASSWORDSALTS',
  privateKeySalts = 'PRIVATEKEYSALTS',
  challenges = 'CHALLENGES'
}

const separator = '$';

export class Redis {
  private static instance: redis.RedisClient;

  constructor() {}

  static connect() {
    console.log('Redis connect()');
    if (this.instance) {
      console.log('il y a déja une instance');
      return this.instance;
    } else {
      console.log("il n'y a pas d'instance");
      this.instance = redis.createClient({ url: process.env.REDIS_URL });
      console.log('this.instance : ', this.instance);
      return this.instance;
    }
  }

  static async set(collection: RedisDatabases, key: string, value: string): Promise<string> {
    console.log('Redis set');
    if (!this.instance) {
      this.connect();
    }

    return new Promise((resolve, reject) => {
      Redis.instance.set(collection + separator + key, value, (err, reply) => {
        if (err) {
          reject(err);
        }
        resolve(reply);
      });
    });
  }

  static async get(collection: RedisDatabases, key: string): Promise<string> {
    console.log('Redis get');
    if (!this.instance) {
      this.connect();
    }

    return new Promise((resolve, reject) => {
      Redis.instance.get(collection + separator + key, (err, reply) => {
        if (err) {
          reject(err);
        }
        resolve(reply);
      });
    });
  }

  static async getAndDelete(collection: RedisDatabases, key: string): Promise<string> {
    console.log('Redis getAndDelete');
    if (!this.instance) {
      this.connect();
    }

    const res = await this.get(collection, key);
    await this.delete(collection, key);
    return res;
  }

  static async delete(collection: RedisDatabases, key: string) {
    console.log('Redis delete');
    if (!this.instance) {
      this.connect();
    }

    return new Promise((resolve, reject) => {
      Redis.instance.del(collection + separator + key, (err, reply) => {
        if (err) {
          reject(err);
        }
        resolve(reply);
      });
    });
  }
}
