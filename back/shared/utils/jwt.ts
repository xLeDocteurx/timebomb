// import { JWTVerifyResponse } from '@interfaces';
import * as dotenv from 'dotenv';
import * as jwt from 'jsonwebtoken';
import { JWTVerifyResponse } from '../../../models/interfaces/index';

dotenv.config({
  path: `../../config/${
    process.env.NODE_ENV ? process.env.NODE_ENV : 'local'
  }.env`
  // debug: true
});

export function generateToken(payload) {
  var token = jwt.sign({ payload }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN
    // algorithm: process.env.JWT_ALGORITHM
  });
  return token;
}

export function decryptToken(token) {
  try {
    var decoded = jwt.verify(token, process.env.JWT_SECRET, {
      // algorithm: process.env.JWT_ALGORITHM
      // algorithms: [process.env.JWT_ALGORITHM]
    });
  } catch (err) {
    throw err;
  }
  return typeof decoded === 'string'
    ? decoded
    : (decoded as JWTVerifyResponse).payload;
}

export function compareTokenToId(token, supposedId) {
  try {
    var decoded = jwt.verify(token, process.env.JWT_SECRET, {
      // algorithm: process.env.JWT_ALGORITHM
      // algorithms: [process.env.JWT_ALGORITHM]
    });
  } catch (err) {
    // return false
    throw err;
  }
  console.log('compareToken decoded', decoded);
  // return decoded._id === supposedId ? true : false;
  return (typeof decoded === 'string'
    ? decoded
    : (decoded as JWTVerifyResponse).payload) === supposedId
    ? true
    : false;
}

export function isTokenAssociatedWithAnUser(token) {}
