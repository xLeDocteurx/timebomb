import * as dotenv from 'dotenv';
import * as socketio from 'socket.io';
import {
  CardRevealedResponse,
  GameLaunchedResponse,
  JoinGameRequestData,
  LaunchGameRequestData,
  LeavedGameResquestData,
  PlayerJoinedResponse,
  PlayerLeavedResponse,
  SendMessageRequestData,
  SocketData
} from '../../../models/interfaces';
import * as controllers from '../../shared/controllers/index';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

// const io = socketio({
//   pingTimeout: process.env.SOCKET_SOCKET_OPTIONS_PING_TIMEOUT,
//   pingInterval: process.env.SOCKET_SOCKET_OPTIONS_PING_INTERVAL
// });

const io = socketio(process.env.SOCKET_WEBPORT);

// io.set('origins', '*:*')
// io.origins('*:*')
// io.set('origins', 'http://yourdomain.com:80')
// io.origins('http://yourdomain.com:80')
// io.origins((origin, callback) => {
// 	if (origin !== '*:*') {
// 		return callback('origin not allowed', false)
// 	}
// 	callback(null, true)
// })

// io.engine.generateId = (req) => {
// 	return clients.length < 1 ? 0 : clients[clients.length - 1].id + 1
// }

// const io_emitter = require('socket.io-emitter')({ host: '127.0.0.1', port: 6379 })
// const EventEmitter = require('events')

io.on('connection', client => {
  console.log('SOCKET ' + client.id + ' // Connected !');
  client.on('initSocketConnection', async (data: SocketData<any>) => {
    console.log('initSocketConnection data : ', data);
    // TODO : remove this check. Fix it from front side.
    if (data?.userToken) {
      const token = data.userToken;
      const user = await controllers.users.getCurrent(token);
      if (user.currentGame) {
        const { userToken } = data;
        const currentGame = await controllers.games.findOneBySlug(userToken, user.currentGame.slug);
        client.join(currentGame.slug);
        console.log(client.id + ' / joined room : ' + currentGame.slug);
      }
    }
  });

  client.on('joinGame', async (payload: SocketData<JoinGameRequestData>) => {
    const token = payload.userToken;
    const { slug } = payload.data;
    const user = await controllers.users.getCurrent(token);
    try {
      var res = await controllers.games.join(token, slug);
    } catch (err) {
      throw err;
    }

    if (user.currentGame) {
      const { userToken } = payload;
      const currentGame = await controllers.games.findOneBySlug(userToken, user.currentGame.slug);
      client.join(currentGame.slug);
      console.log(client.id + ' / joined room : ' + currentGame.slug);
    }

    const resPayload: PlayerJoinedResponse = {
      res: res
    };

    io.in(slug).emit('playerJoined', resPayload);
  });

  client.on('leaveGame', async (payload: SocketData<LeavedGameResquestData>) => {
    const token = payload.userToken;
    const { slug } = payload.data;
    const user = await controllers.users.getCurrent(token);
    try {
      var res = await controllers.games.leave(token, slug);
    } catch (err) {
      throw err;
    }

    if (user.currentGame) {
      const { userToken } = payload;
      const currentGame = await controllers.games.findOneBySlug(userToken, user.currentGame.slug);
      client.leave(currentGame.slug);
      console.log(client.id + ' / joined room : ' + currentGame.slug);
    }

    const resPayload: PlayerLeavedResponse = {
      res: res
    };

    io.in(slug).emit('playerLeaved', resPayload);
  });

  client.on('launchGame', async (payload: SocketData<LaunchGameRequestData>) => {
    const token = payload.userToken;
    const { slug } = payload.data;
    try {
      var res = await controllers.games.launch(token, slug);
    } catch (err) {
      throw err;
    }

    const resPayload: GameLaunchedResponse = {
      res: res
    };

    io.in(slug).emit('gameLaunched', resPayload);
  });

  client.on('sendMessage', async (payload: SocketData<SendMessageRequestData>) => {
    const { slug, message } = payload.data;
    const user = await controllers.users.getCurrent(payload.userToken);
    message.sender = user.username;
    client.to(slug).emit('receiveMessage', message);
    // io.in(slug).emit('receiveMessage', message);
    // io.emit('receiveMessage', message);
  });

  client.on('revealCard', async data => {
    const token = data.userToken;
    const { playerId, cardIndex, slug } = data.data;
    try {
      var revealedCard = await controllers.games.revealCard(token, playerId);
    } catch (err) {
      throw err;
    }

    const resPayload: CardRevealedResponse = {
      cardType: revealedCard,
      cardIndex: cardIndex
    };

    io.in(slug).emit('cardRevealed', resPayload);
  });

  client.on('disconnect', reason => {
    console.log('SOCKET ' + client.id + ' // Got disconnect ! // reason : ' + reason);
  });

  client.on('error', error => {
    console.log('SOCKET Error : ', error);
  });
});

// io.listen(process.env.SOCKET_WEBPORT);

console.log('back/socket is running on port : ', process.env.SOCKET_WEBPORT);
