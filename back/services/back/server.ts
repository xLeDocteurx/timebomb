import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as http from 'http';
import * as controllers from '../../shared/controllers/index';
import { checkPermissions } from '../../shared/middlewares/index';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

const app = express();
const server = http.createServer(app);
server.timeout = 2000;

server.listen(process.env.BACK_WEBPORT);
// 	.addListener('connection', value => {
//   console.log('value : ', value);
// });

app.use(cors());
app.options('*', cors());
app.use(express.json()); // to support JSON-encoded bodies

app.use('/users', checkPermissions);
app.get('/users', async (req, res) => {
  try {
    var returnedUsers = await controllers.users.findAll();
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedUsers);
});

app.use('/users/:username', checkPermissions);
app.get('/users/:username', async (req, res) => {
  const username = req.params.username;

  try {
    var returnedUser = await controllers.users.findOneByUsername(username);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedUser);
});

app.use('/game', checkPermissions);
app.get('/game', async (req, res) => {
  try {
    var returnedGames = await controllers.games.getCurrent(req.headers);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGames);
});

app.use('/game/revealCard', checkPermissions);
app.post('/game/revealCard', async (req, res) => {
  try {
    var returnedGames = await controllers.games.revealCard(req.headers, req.body.playerId);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGames);
});

app.use('/games/availables', checkPermissions);
app.get('/games/availables', async (req, res) => {
  try {
    var returnedGames = await controllers.games.findAllAvailables();
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGames);
});

app.use('/games/create', checkPermissions);
app.post('/games/create', async (req, res) => {
  try {
    var returnedGame = await controllers.games.create({
      options: req.body.options,
      authorization: req.headers.authorization
    });
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGame);
});

app.use('/games/join', checkPermissions);
app.post('/games/join', async (req, res) => {
  try {
    var returnedGame = await controllers.games.join(req.headers, req.body.slug);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGame);
});

app.use('/games/launch', checkPermissions);
app.post('/games/launch', async (req, res) => {
  try {
    var returnedGame = await controllers.games.launch(req.headers, req.body.slug);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGame);
});

app.use('/games/leave', checkPermissions);
app.post('/games/leave', async (req, res) => {
  try {
    var returnedGame = await controllers.games.leave(req.headers, req.body.slug);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGame);
});

app.use('/games/:slug', checkPermissions);
app.get('/games/:slug', async (req, res) => {
  const slug = req.params.slug;

  try {
    var returnedGame = await controllers.games.findOneBySlug(req.headers, slug);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedGame);
});

app.get('/*', (req, res) => {
  res.status(404).send('API. Not Found baby !');
});

console.log('back/back is running on port : ', process.env.AUTH_WEBPORT);
