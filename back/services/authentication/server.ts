import * as cors from 'cors';
import * as dotenv from 'dotenv';
import * as express from 'express';
import * as http from 'http';
import * as controllers from '../../shared/controllers/index';
import { checkPermissions } from '../../shared/middlewares/index';

dotenv.config({
  path: `../../config/${process.env.NODE_ENV ? process.env.NODE_ENV : 'local'}.env`
  // debug: true
});

const app = express();
const server = http.createServer(app);
server.timeout = 2000;

server.listen(process.env.AUTH_WEBPORT);
// 	.addListener('connection', value => {
//   console.log('value : ', value);
// });

app.use(cors());
app.options('*', cors());
app.use(express.json()); // to support JSON-encoded bodies

app.get('/register', async (req, res) => {
  console.log('-------------');
  console.log('get /register');
  console.log('-------------');
  try {
    var passwordAndPrivateKeySalts = await controllers.auth.getPasswordAndPrivateKeySalts();
  } catch (err) {
    console.log('get register catch err :  ', err);
    res.status(400).json({ error: err.toString() });
  }
  res.status(200).send(passwordAndPrivateKeySalts);
});

app.post('/register', async (req, res) => {
  console.log('-------------');
  console.log('post /register');
  console.log('-------------');
  try {
    var returnedUserAndToken = await controllers.auth.register({
      username: req.body.username,
      email: req.body.email,
      avatarType: req.body.avatarType,
      passwordHash: req.body.passwordHash,
      passwordSaltId: req.body.passwordSaltId,
      publicKey: req.body.publicKey,
      encryptedPrivate: req.body.encryptedPrivate,
      privateKeySaltId: req.body.privateKeySaltId
    });
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedUserAndToken);
});

app.post('/getchallenge', async (req, res) => {
  console.log('-------------');
  console.log('post /getchallenge');
  console.log('-------------');
  try {
    var passwordSaltsAndChallenge = await controllers.auth.getPasswordSaltsAndChallenge({
      email: req.body.email
    });
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }
  res.status(200).send(passwordSaltsAndChallenge);
});

app.post('/connect', async (req, res) => {
  console.log('-------------');
  console.log('post /connect');
  console.log('-------------');
  try {
    var returnedUserAndToken = await controllers.auth.connect({
      email: req.body.email,
      passwordHash: req.body.passwordHash,
      passwordChallengeId: req.body.passwordChallengeId
    });
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedUserAndToken);
});

app.use('/user', checkPermissions);
app.get('/user', async (req, res) => {
  console.log('-------------');
  console.log('get /user');
  console.log('-------------');
  try {
    var returnedUser = await controllers.users.getCurrent(req.headers);
  } catch (err) {
    res.status(400).json({ error: err.toString() });
  }

  res.status(200).send(returnedUser);
});

app.get('/*', (req, res) => {
  console.log('-------------');
  console.log('/*');
  console.log('-------------');
  console.log('JSON.stringify(req.headers) : ', JSON.stringify(req.headers));
  console.log('-------------');
  console.log('JSON.stringify(req.body) : ', JSON.stringify(req.body));
  console.log('-------------');
  console.log('-------------');
  res.status(404).send('Auth. Not Found baby !');
});

console.log('back/auth is running on port : ', process.env.AUTH_WEBPORT);
