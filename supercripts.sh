# scp supercripts.sh root@91.234.195.89:supercripts.sh
# chmod +x ./supercripts.sh

sudo apt update
sudo apt upgrade

# docker
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update

sudo apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update

sudo apt-get -y install docker-ce docker-ce-cli containerd.io

docker --version

# docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

docker-compose --version

# # minecraft pocketmine-mp
# mkdir data plugins

# sudo chown -R 1000:1000 data plugins

# docker run -it -p 19132:19132 -v $PWD/data:/data -v $PWD/plugins:/plugins pmmp/pocketmine-mp

# # minecraft bedrock
# wget https://raw.githubusercontent.com/TheRemote/MinecraftBedrockServer/master/SetupMinecraft.sh

# chmod +x SetupMinecraft.sh

# ./SetupMinecraft.sh

# Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

# Give it permissions to execute
sudo chmod +x /usr/local/bin/gitlab-runner

# Create a GitLab CI user
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# Install and run as service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start

# register gitlab runner
# sudo gitlab-runner register --url https://gitlab.com/ --registration-token 6bHckpHYfjaoH975a9ES
# sudo gitlab-runner register -n \ --url https://gitlab.com/ \ --registration-token 6bHckpHYfjaoH975a9ES \ --description "Timebom runner"
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token 6bHckpHYfjaoH975a9ES \
  --executor shell \
  --description "Timebom runner"

# add gitlab-runner to docker permissions
sudo usermod -aG docker gitlab-runner

sudo rm -rf /home/gitlab-runner/.bash_logout

sudo rm -rf /home/gitlab-runner/.bashrc


# # # give sudo permission to docker
# # sudo chmod 666 /var/run/docker.sock

# # add gitlab-runner to sudoers
# # gitlab-runner: ALL-(ALL) NOPASSWD: ALL
# nano /etc/sudoers
sudo usermod -a -G sudo gitlab-runner
echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.tmp

# # redis
# sudo apt-get -y install redis-server
# sudo systemctl enable redis-server.service

# touch /etc/redis/redis.conf
# echo 'bind 127.0.0.1 -::1' > /etc/redis/redis.conf
# echo 'protected-mode yes' >> /etc/redis/redis.conf
# echo 'port 6379' >> /etc/redis/redis.conf
# echo 'tcp-backlog 511' >> /etc/redis/redis.conf
# echo 'timeout 0' >> /etc/redis/redis.conf
# echo 'tcp-keepalive 300' >> /etc/redis/redis.conf
# echo 'daemonize no' >> /etc/redis/redis.conf
# echo 'pidfile /var/run/redis_6379.pid' >> /etc/redis/redis.conf
# echo 'loglevel notice' >> /etc/redis/redis.conf
# echo 'logfile ""' >> /etc/redis/redis.conf
# echo 'databases 16' >> /etc/redis/redis.conf
# echo 'always-show-logo no' >> /etc/redis/redis.conf
# echo 'set-proc-title yes' >> /etc/redis/redis.conf
# echo 'proc-title-template "{title} {listen-addr} {server-mode}"' >> /etc/redis/redis.conf
# echo 'stop-writes-on-bgsave-error yes' >> /etc/redis/redis.conf
# echo 'rdbcompression yes' >> /etc/redis/redis.conf
# echo 'rdbchecksum yes' >> /etc/redis/redis.conf
# echo 'dbfilename dump.rdb' >> /etc/redis/redis.conf
# echo 'rdb-del-sync-files no' >> /etc/redis/redis.conf
# echo 'dir ./' >> /etc/redis/redis.conf
# echo 'replica-serve-stale-data yes' >> /etc/redis/redis.conf
# echo 'replica-read-only yes' >> /etc/redis/redis.conf
# echo 'repl-diskless-sync no' >> /etc/redis/redis.conf
# echo 'repl-diskless-sync-delay 5' >> /etc/redis/redis.conf
# echo 'repl-diskless-load disabled' >> /etc/redis/redis.conf
# echo 'repl-disable-tcp-nodelay no' >> /etc/redis/redis.conf
# echo 'replica-priority 100' >> /etc/redis/redis.conf
# echo 'acllog-max-len 128' >> /etc/redis/redis.conf
# echo 'maxmemory 256mb' >> /etc/redis/redis.conf
# echo 'maxmemory-policy allkeys-lru' >> /etc/redis/redis.conf
# echo 'lazyfree-lazy-eviction no' >> /etc/redis/redis.conf
# echo 'lazyfree-lazy-expire no' >> /etc/redis/redis.conf
# echo 'lazyfree-lazy-server-del no' >> /etc/redis/redis.conf
# echo 'replica-lazy-flush no' >> /etc/redis/redis.conf
# echo 'lazyfree-lazy-user-del no' >> /etc/redis/redis.conf
# echo 'lazyfree-lazy-user-flush no' >> /etc/redis/redis.conf
# echo 'oom-score-adj no' >> /etc/redis/redis.conf
# echo 'oom-score-adj-values 0 200 800' >> /etc/redis/redis.conf
# echo 'disable-thp yes' >> /etc/redis/redis.conf
# echo 'appendonly no' >> /etc/redis/redis.conf
# echo 'appendfilename "appendonly.aof"' >> /etc/redis/redis.conf
# echo 'appendfsync everysec' >> /etc/redis/redis.conf
# echo 'no-appendfsync-on-rewrite no' >> /etc/redis/redis.conf
# echo 'auto-aof-rewrite-percentage 100' >> /etc/redis/redis.conf
# echo 'auto-aof-rewrite-min-size 64mb' >> /etc/redis/redis.conf
# echo 'aof-load-truncated yes' >> /etc/redis/redis.conf
# echo 'aof-use-rdb-preamble yes' >> /etc/redis/redis.conf
# echo 'lua-time-limit 5000' >> /etc/redis/redis.conf
# echo 'slowlog-log-slower-than 10000' >> /etc/redis/redis.conf
# echo 'slowlog-max-len 128' >> /etc/redis/redis.conf
# echo 'latency-monitor-threshold 0' >> /etc/redis/redis.conf
# echo 'notify-keyspace-events ""' >> /etc/redis/redis.conf
# echo 'hash-max-ziplist-entries 512' >> /etc/redis/redis.conf
# echo 'hash-max-ziplist-value 64' >> /etc/redis/redis.conf
# echo 'list-max-ziplist-size -2' >> /etc/redis/redis.conf
# echo 'list-compress-depth 0' >> /etc/redis/redis.conf
# echo 'set-max-intset-entries 512' >> /etc/redis/redis.conf
# echo 'zset-max-ziplist-entries 128' >> /etc/redis/redis.conf
# echo 'zset-max-ziplist-value 64' >> /etc/redis/redis.conf
# echo 'hll-sparse-max-bytes 3000' >> /etc/redis/redis.conf
# echo 'stream-node-max-bytes 4096' >> /etc/redis/redis.conf
# echo 'stream-node-max-entries 100' >> /etc/redis/redis.conf
# echo 'activerehashing yes' >> /etc/redis/redis.conf
# echo 'client-output-buffer-limit normal 0 0 0' >> /etc/redis/redis.conf
# echo 'client-output-buffer-limit replica 256mb 64mb 60' >> /etc/redis/redis.conf
# echo 'client-output-buffer-limit pubsub 32mb 8mb 60' >> /etc/redis/redis.conf
# echo 'hz 10' >> /etc/redis/redis.conf
# echo 'dynamic-hz yes' >> /etc/redis/redis.conf
# echo 'aof-rewrite-incremental-fsync yes' >> /etc/redis/redis.conf
# echo 'rdb-save-incremental-fsync yes' >> /etc/redis/redis.conf
# echo 'jemalloc-bg-thread yes' >> /etc/redis/redis.conf

sudo systemctl restart redis-server.service

# mongodb
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
sudo apt-get -y install gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
sudo apt-get update
sudo apt-get -y install -y mongodb-org
echo "mongodb-org hold" | sudo dpkg --set-selections
echo "mongodb-org-server hold" | sudo dpkg --set-selections
echo "mongodb-org-shell hold" | sudo dpkg --set-selections
echo "mongodb-org-mongos hold" | sudo dpkg --set-selections
echo "mongodb-org-tools hold" | sudo dpkg --set-selections

sudo systemctl enable mongod.service






# sudo add-apt-repository ppa:certbot/certbot
# sudo apt-get update
# sudo apt-get -y install certbot

sudo apt update
sudo apt -y install nginx

touch /var/www/index.html
echo '<!doctype html><html><head><meta charset="utf-8"><title>Hello, Nginx!</title></head><body>' > /var/www/index.html
echo '<h1>Hello, My Nginx!</h1>' >> /var/www/index.html
echo '<p>We have just configured our Nginx web server on Ubuntu Server!</p>' >> /var/www/index.html
echo '</body></html>' >> /var/www/index.html

touch /etc/nginx/conf.d/default.conf
echo 'server {' > /etc/nginx/conf.d/default.conf
echo '    listen 80;' >> /etc/nginx/conf.d/default.conf
echo '    listen [::]:80;' >> /etc/nginx/conf.d/default.conf
# echo '    server_name timebom.be *.timebom.be;' >> /etc/nginx/conf.d/default.conf
echo '    server_name timebom.be;' >> /etc/nginx/conf.d/default.conf
echo 'location / {' >> /etc/nginx/conf.d/default.conf
echo '      root /var/www/;' >> /etc/nginx/conf.d/default.conf
echo '      index index.html;' >> /etc/nginx/conf.d/default.conf
echo '      try_files $uri $uri/ /index.html =404;' >> /etc/nginx/conf.d/default.conf
echo '    }' >> /etc/nginx/conf.d/default.conf
echo '}' >> /etc/nginx/conf.d/default.conf

sudo nginx -s reload

sudo apt update
sudo apt -y install snapd
sudo snap install core; sudo snap refresh core

sudo snap install --classic certbot
sudo apt-get install -y python3-certbot-nginx
# sudo ln -s /snap/bin/cerbot /usr/bin/certbot
sudo certbot --nginx

sudo nginx -s reload

rm -rf /etc/nginx/conf.d/default.conf
touch /etc/nginx/conf.d/default.conf
echo 'server {' > /etc/nginx/conf.d/default.conf
echo '    server_name timebom.be;' >> /etc/nginx/conf.d/default.conf
echo '    listen [::]:443 ssl ipv6only=on; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    listen 443 ssl; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    ssl_certificate /etc/letsencrypt/live/timebom.be/fullchain.pem; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    ssl_certificate_key /etc/letsencrypt/live/timebom.be/privkey.pem; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    location / {' >> /etc/nginx/conf.d/default.conf
echo '      proxy_pass http://timebom.be:8080;' >> /etc/nginx/conf.d/default.conf
echo '    }' >> /etc/nginx/conf.d/default.conf
echo '    location /api/ {' >> /etc/nginx/conf.d/default.conf
echo '        add_header Access-Control-Allow-Origin *;' >> /etc/nginx/conf.d/default.conf
echo '        add_header Access-Control-Expose-Headers "Origin";' >> /etc/nginx/conf.d/default.conf
echo '        rewrite /api/(.*) /$1 break;' >> /etc/nginx/conf.d/default.conf
echo '        proxy_pass http://185.98.136.22:1337;' >> /etc/nginx/conf.d/default.conf
echo '    }' >> /etc/nginx/conf.d/default.conf
echo '    location /socket/ {' >> /etc/nginx/conf.d/default.conf
echo '        add_header Access-Control-Allow-Origin *;' >> /etc/nginx/conf.d/default.conf
echo '        add_header Access-Control-Expose-Headers "Origin";' >> /etc/nginx/conf.d/default.conf
echo '        rewrite /socket/(.*) /$1 break;' >> /etc/nginx/conf.d/default.conf
echo '        proxy_pass http://185.98.136.22:1338;' >> /etc/nginx/conf.d/default.conf
echo '    }' >> /etc/nginx/conf.d/default.conf
echo '    location /auth/ {' >> /etc/nginx/conf.d/default.conf
echo '        add_header Access-Control-Allow-Origin *;' >> /etc/nginx/conf.d/default.conf
echo '        add_header Access-Control-Expose-Headers "Origin";' >> /etc/nginx/conf.d/default.conf
echo '        rewrite /auth/(.*) /$1 break;' >> /etc/nginx/conf.d/default.conf
echo '        proxy_pass http://185.98.136.22:1339;' >> /etc/nginx/conf.d/default.conf
echo '    }' >> /etc/nginx/conf.d/default.conf
echo '}' >> /etc/nginx/conf.d/default.conf
echo 'server {' >> /etc/nginx/conf.d/default.conf
echo '    if ($host = timebom.be) {' >> /etc/nginx/conf.d/default.conf
echo '        return 301 https://$host$request_uri;' >> /etc/nginx/conf.d/default.conf
echo '    } # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '    listen 80;' >> /etc/nginx/conf.d/default.conf
echo '    listen [::]:80;' >> /etc/nginx/conf.d/default.conf
echo '    server_name timebom.be;' >> /etc/nginx/conf.d/default.conf
echo '    return 404; # managed by Certbot' >> /etc/nginx/conf.d/default.conf
echo '}' >> /etc/nginx/conf.d/default.conf

sudo nginx -s reload

# sudo mkdir ~/nginx-ssl
# sudo mkdir ~/nginx-ssl/conf.d
# touch ~/nginx-ssl/conf.d/default.conf
# echo 'server {' > ~/nginx-ssl/conf.d/default.conf
# echo '     listen [::]:80;' >> ~/nginx-ssl/conf.d/default.conf
# echo '     listen 80;' >> ~/nginx-ssl/conf.d/default.conf
# echo '' >> ~/nginx-ssl/conf.d/default.conf
# echo '     server_name timebom.be www.timebom.be;' >> ~/nginx-ssl/conf.d/default.conf
# echo '' >> ~/nginx-ssl/conf.d/default.conf
# echo '     location ~ /.well-known/acme-challenge {' >> ~/nginx-ssl/conf.d/default.conf
# echo '         allow all;' >> ~/nginx-ssl/conf.d/default.conf
# echo '         root /var/www/certbot;' >> ~/nginx-ssl/conf.d/default.conf
# echo '     }' >> ~/nginx-ssl/conf.d/default.conf
# echo '}' >> ~/nginx-ssl/conf.d/default.conf

# touch ~/nginx-ssl/docker-compose.yml
# echo 'version: "3.8"' > ~/nginx-ssl/docker-compose.yml
# echo 'services:' >> ~/nginx-ssl/docker-compose.yml
# echo '    web:' >> ~/nginx-ssl/docker-compose.yml
# echo '        image: nginx:latest' >> ~/nginx-ssl/docker-compose.yml
# echo '        restart: always' >> ~/nginx-ssl/docker-compose.yml
# echo '        volumes:' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./public:/var/www/html' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./conf.d:/etc/nginx/conf.d' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./certbot/conf:/etc/nginx/ssl' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./certbot/data:/var/www/certbot' >> ~/nginx-ssl/docker-compose.yml
# echo '        ports:' >> ~/nginx-ssl/docker-compose.yml
# echo '            - 80:80' >> ~/nginx-ssl/docker-compose.yml
# echo '            - 443:443' >> ~/nginx-ssl/docker-compose.yml
# echo '    certbot:' >> ~/nginx-ssl/docker-compose.yml
# echo '        image: certbot/certbot:latest' >> ~/nginx-ssl/docker-compose.yml
# echo '        command: certonly --webroot --webroot-path=/var/www/certbot --email admin@timebom.be --agree-tos --no-eff-email -d timebom.be -d www.timebom.be' >> ~/nginx-ssl/docker-compose.yml
# echo '        volumes:' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./certbot/conf:/etc/letsencrypt' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./certbot/logs:/var/log/letsencrypt' >> ~/nginx-ssl/docker-compose.yml
# echo '            - ./certbot/data:/var/www/certbot' >> ~/nginx-ssl/docker-compose.yml

# cd ~/nginx-ssl/
# docker-compose up -d
# docker-compose ps
# cd ~/
