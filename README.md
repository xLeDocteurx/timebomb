# timebomb

Timebomb est un jeu de cartes explosif !

### Node web

From root folder, for dev purpose

```
npm run start
```

Lerna will bootstrap the project as it is a mono repo

### Node desktop

- dev :

```

```

- build :

```

```

### Docker

From root folder, for prod testing

- start :

```
docker-compose up -d
```

- stop :

```
docker-compose down
```
