# TO DO

- utiliser les variables docker pour les fichiers de connfig du back. ( notament celles de redis )
- Un npm install qui marche a la racine
- Gérer la manière dont les services se comportent avant la connection ( socket / back et auth )
- Gérer les "Cannot set headers after they are sent to the client"
- Gérer les "TODO" et "TO DO".

- des sous domaines pour les différents services. ( https, etc ... )
- le https sur le serveur. ( lets encrypt, etc.. )
- revoir la gestion des cookies ( faire du vrai http only )
- s'occuper du crypto service et de la récupération des clefs dans l'auth service
- Rajouter les algos par défaut dans les fichiers d'environement
- rajouter la sauvegarde et l'utilisation de l'algo de hashage pour la clef privée

- Régler le problème des exports coté angular pour avoir des utils propres.

- Inclure une durée de vie max des entrées de la base de données redis.
- Typer les retour de la classe redis.

- Deplacer les docker compose dans les dossiers des différents services.

- Dans le lobby indiquer une fraction du nombre de participants sur le nombre de place maximale pour chaque partie

- !!! BUG SUR LES TEAMS. J'ai eu une partie sans joueur rouge !!!

<!-- - Pour le mélange des cartes:
  . Séparer le mélange avec la bombe du mélange des difuses
  . Utiliser le bad shuffle pour le mélange des difuses -->

- preview de l'avatar au register
- déplacer le fichier main.js de web vers electron ( a modifier )

- configurer connection ssh au serveur
- Controller le timeout des sockets.

- du dynamique component loading pour les messages (Faire un shared component pour le chat)
- persistance des messages
- une date d'envoi des messages
- Un observable avec les messages qui se poussent dans un array via pipe puis map
- faire les "souscriptions" socket io dans les services associés

- epurer les dotenv.config( inutiles)
- Faire des interfaces pour les data qui transitent via socket et les réponses des services / controllers

- séparer et organiser les controllers en dossier ( games, user, etc.. ) et faire un fichier par fonction
- travailler sur le title de l'application et des pages ( passer un arg data dans les routeurs pour personnaliser la route )
- trouver une solution pour les variables d'environement de type number et le "as unknown as number"
- revoir les docker file de max pour comprendre comment il importe des contenu à l'extérieur du dossier ou se trouve le dockerfile
- dans le game service : virer le slug des params et le remplacer par this.currentgameslug
- Recherche de joueur
- Indicateur d'activité du profil
- Ajout à la liste d'amis
- certificats ssl ( letsencrypt )
- Si la game n'éxiste pas redirection au lobby ? (plutot qu'affichage d'une vue)
- améliorer le logout pour ne pas avoir a faire de refresh
- Le refetch dans, notament, le lobbyService qui ne refetch pas
- Redirection vers la game apres l'avoir créée
- Un success service ( identique à l'error sercvice )
- le bug au changement de compte qui garde en mémoire la current game précédente ( voir les données de l'utilisateur précédent )
- du Promise.all dans les fonctionctions de population des controllers
- virer tous les mongoose des packages json
- controller les valeurs de retours des controllers ( depuis les collections jusqu'aux socket&bback en passant par les controlleurs)
- pas de throw error sur le leave the game
- des interfaces Params pour les requettes sockets
- des interfaces Params pour les requettes mongodb
- checker comment est fait le 'Mongo.close()' sur max
- virer mongoose et le remplacer par mongodb
- s'occuper des variables css de couleurs ( repasser au hex et pas au rgb )
- déplacer les dependance de @Type vers les devDependances
- wtf le refetch dans le games controller
- section "your games"
- filtrer les games dans le lobby pour ne pas afficher celles dont nous sommes déja membre ?
- game service : creategame & joingame generique et retour de type game pour selection auto
- back : remettre le checkpermissions sur la route ?
- Des ObjectId à la place des string dans les models référançant les Ids d'entités
- typer les inputs
- utiliser un reducer ( et/ou reduceRight ) dans la methode shuffle
- regarder les typages des servies networks
- un getUser() dans le auth service avec persistance des datas
- un log out
- un error service
- le loading service
- un if dans la fonction mutate pour le loadingservice
- imports de styles globaux
- Des guards pour les routes
- Pagination des résultats
- Ajouter une phrase dans le profil si l'utilisateur utilise gravatar pour aller changer sa photo de profil
- Faire un proxy pour pointer vers les serveurs depuis l'adresse/le port du front
- Path pour les modules persos (tsconfig)
- Variables d'environement ( dotenv ) :
- webports, cors options, database adresses
- docker compose
- gitlab ci cd
- Traduction coté front
- Traduction coté front des messages venant du back ( erreurs notament )
- Reproduire le système d'authentication de botdesign ( challenge, cryto coté front et back )
- bdd redis ( pour le challenge de l'auth puis pour les token des mails )
- réinitialisation de mot de passe ( redis )
- Faire des variables pour les fonts
- Un système de souscription dans le loading service
- Des notifications (socket + stack)
- un systeme de modals
- Un module de layout
- Accéssibilité et balises
- cron pour la confirmation du compte
- get rid of axios
- le routerlinkactive : https://www.google.com/search?q=angular+routerLinkActive&rlz=1C5CHFA_enFR888FR889&oq=angular+routerLinkActive&aqs=chrome..69i57j0l7.3367j0j7&sourceid=chrome&ie=UTF-8
- validators pour le nombre max de joueurs sur une game : https://stackoverflow.com/questions/28514790/how-to-set-limit-for-array-size-in-mongoose-schema
- les private dans les constructeur de service network

* A 'super' call must be the first statement in the constructor when a class contains initialized properties, parameter properties, or private identifiers.
  ( auth service )
* WTF with nodemon
* WTF with observable refetch
