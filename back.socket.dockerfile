FROM mhart/alpine-node:12 AS build
# FROM node:10 AS build
# FROM node:12.0 AS build
LABEL stage=builder

RUN mkdir -p /app

COPY ./back/services/socket /app/back/services/socket

COPY ./utils /app/utils

COPY ./back/shared /app/back/shared

WORKDIR /app/utils

RUN npm install

WORKDIR /app/back/shared

RUN npm install

COPY ./models /app/models

WORKDIR /app/models

RUN npm install

WORKDIR /app/back/services/socket

RUN npm install

ENV NODE_ENV prod

RUN npm run build

FROM mhart/alpine-node:12 AS modules
# FROM node:10 AS modules
LABEL stage=builder

RUN mkdir -p /app
RUN mkdir -p /app/utils
RUN mkdir -p /app/back/shared
RUN mkdir -p /app/models
RUN mkdir -p /app/back/services/socket

WORKDIR /app/utils

COPY ./utils/package.json /app/utils
COPY ./utils/package-lock.json /app/utils

RUN npm install --production

WORKDIR /app/back/shared

COPY ./back/shared/package.json /app/back/shared
COPY ./back/shared/package-lock.json /app/back/shared

RUN npm install --production

WORKDIR /app/models

COPY ./models/package.json /app/models
COPY ./models/package-lock.json /app/models

RUN npm install --production

WORKDIR /app/back/services/socket

COPY ./back/services/socket/package.json /app/back/services/socket
COPY ./back/services/socket/package-lock.json /app/back/services/socket

RUN npm install --production

FROM alpine:latest AS app
LABEL stage=builder

RUN mkdir -p /app

COPY --from=build /app/back/services/socket/dist /app

COPY --from=modules /app/utils/node_modules /app/utils/node_modules
COPY --from=modules /app/back/shared/node_modules /app/back/shared/node_modules
COPY --from=modules /app/models/node_modules /app/models/node_modules
COPY --from=modules /app/back/services/socket/node_modules /app/back/services/socket/node_modules

# FROM gcr.io/distroless/nodejs:debug
FROM gcr.io/distroless/nodejs
# docker build -t my_debug_image .
# docker run --entrypoint=sh -ti my_debug_image

# RUN mkdir -p /app

COPY ./back/config /app/back/config

COPY --from=app /app /app

WORKDIR /app/back/services/socket

EXPOSE 1338

ENV NODE_ENV prod

CMD ["server.js"]
