import { Challenge, PasswordSalt } from '..';

export interface GetPasswordSaltsAndChallengeResponse {
  passwordSalt: PasswordSalt;
  challenge: Challenge;
}
