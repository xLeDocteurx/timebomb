import { PrivateKeySalt } from '../salts.interface';

export interface ConnectResponse {
  token: string;
  keys: {
    public: string;
    encryptedPrivate: {
      value: string;
      salt: PrivateKeySalt;
    };
  };
}
