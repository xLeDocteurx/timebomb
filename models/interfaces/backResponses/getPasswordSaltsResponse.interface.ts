import { PasswordSalt } from '..';
import { PrivateKeySalt } from '../salts.interface';

export interface GetPasswordAndPrivateKeySaltsResponse {
  passwordSalt: PasswordSalt;
  privateKeySalt: PrivateKeySalt;
}
