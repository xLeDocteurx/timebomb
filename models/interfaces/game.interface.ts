import { ObjectId } from 'mongodb';
import { GameStatus, GameTypes, PlayerTeams } from '../enums';
import { GameState, PublicGameState } from './gameState.interface';
import { PublicUser } from './user.interface';

export interface GameInput {
  options: { name: string };
  authorization?: string;
}

export interface PublicGame {
  name: string;
  slug: string;
  type: GameTypes;

  status: GameStatus;
  state?: PublicGameState;
  userTeam?: PlayerTeams;
  userNumerofDiffuse?: number;
  userHasTheBomb?: boolean;
  // result: {};

  creator: PublicUser;
  players: PublicUser[];

  created: Date;
}

export interface Game {
  _id?: ObjectId;

  name: string;
  slug: string;
  type: GameTypes;

  status: GameStatus;
  state?: GameState;
  // result: {};

  // creator: PublicUser;
  // players: PublicUser[];
  creator: ObjectId;
  players: ObjectId[];

  created: Date;
  started: Date;
  ended: Date;
}
