import { APIS, APISMethods } from '../enums/';

export interface APIRequest {
  target: APIS;
  method: APISMethods;
  url: string;
}
