export interface JWTVerifyResponse {
  header: any;
  payload: string;
  signature: string;
}
