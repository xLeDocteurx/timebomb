export interface JWTDecodeResponse {
  header: any;
  payload: string;
  signature: string;
}
