import { Dic } from './dic.interface';

export type DicRec<T> = Dic<T | DicRec<T>>;
