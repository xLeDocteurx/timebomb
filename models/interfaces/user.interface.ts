import { ObjectId } from 'mongodb';
import { PasswordSalt, PrivateKeySalt } from '.';
import { AvatarTypes, Roles } from '../enums';
import { PublicGame } from './game.interface';

export interface UserInputRegisterLight {
  username: string;
  email: string;
  avatarType: string;
  password1: string;
  password2: string;
}

export interface UserInputRegister {
  username: string;
  email: string;
  avatarType: string;
  passwordHash: string;
  passwordSaltId: string;
  publicKey: string;
  encryptedPrivate: string;
  privateKeySaltId: string;
}

export interface GetPasswordSaltsAndChallengeInput {
  email: string;
}

export interface UserInputConnectLight {
  email: string;
  password: string;
}

export interface UserInputConnect {
  email: string;
  passwordHash: string;
  passwordChallengeId: string;
}

export interface PrivateUser {
  _id: ObjectId;
  username: string;
  email: string;

  gravatarHash: string;
  avatarType: AvatarTypes;

  role: Roles;
  confirmed: boolean;
  currentGame: PublicGame;
  friends: PublicUser[];
  games: number;
  wins: number;
}
export interface PublicUser {
  _id: ObjectId;
  username: string;

  gravatarHash: string;
  avatarType: AvatarTypes;

  role: Roles;
  currentGame: PublicGame;
  games: number;
  wins: number;
}

export interface User {
  _id?: ObjectId;

  username: string;
  email: string;

  gravatarHash: string;
  avatarType: AvatarTypes;

  auth: {
    password: {
      hash: string;
      salt: PasswordSalt;
    };
    keys: {
      publics: {
        current: string;
        previous?: string[];
      };
      encryptedPrivate: {
        value: string;
        salt: PrivateKeySalt;
      };
    };
  };

  role: Roles;
  confirmed: boolean;
  currentGame: ObjectId;
  friends: ObjectId[];
  games: number;
  wins: number;
}
