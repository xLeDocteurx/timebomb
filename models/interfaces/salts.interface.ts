import * as forge from 'node-forge';
import { HashAlgType } from '../../utils/crypto/enums';

export interface PasswordSalt {
  redisId?: string;
  value: string;
  iterations: number;
  keyAlgs: {
    hash: HashAlgType;
    hmac: forge.hmac.Algorithm;
  };
}

export interface PrivateKeySalt {
  redisId?: string;
  value: string;
  iterations: number;
}
