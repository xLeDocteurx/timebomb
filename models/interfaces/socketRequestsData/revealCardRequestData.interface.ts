export interface RevealCardRequestData {
  slug: string;
  playerId: string;
  cardIndex: number;
}
