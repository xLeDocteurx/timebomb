import { SocketOutgoingMessage } from '../../interfaces';

export interface SendMessageRequestData {
  message: SocketOutgoingMessage;
  slug: string;
}
