export interface SocketIncomingMessage {
  sender: string;
  content: string;
}

export interface SocketOutgoingMessage {
  sender?: string;
  content: string;
}
