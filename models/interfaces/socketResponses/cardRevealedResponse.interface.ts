import { CardsTypes } from '../../enums';

export interface CardRevealedResponse {
  cardType: CardsTypes;
  cardIndex: number;
}
