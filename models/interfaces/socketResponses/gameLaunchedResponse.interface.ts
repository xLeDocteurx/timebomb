import { UpdateWriteOpResult } from 'mongodb';

export interface GameLaunchedResponse {
  res: UpdateWriteOpResult;
}
