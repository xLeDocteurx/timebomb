export interface SocketRequest<T> {
  event: string;
  payload: SocketData<T>;
}

export interface SocketData<T> {
  userToken?: string;
  data: T;
}
