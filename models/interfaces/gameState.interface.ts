import { ObjectId } from 'mongodb';
import { Dic } from '.';
import { PlayerTeams } from '../enums';

// export interface PrivateGameState {
//   turn: number;
//   playerToPlay: ObjectId;
//   players: GameStatePlayer | PublicGameStatePlayer[];
//   cardsOut: number;
//   diffuseOut: number;
// }
export interface PublicGameState {
  turn: number;
  newTurn?: boolean;
  playerToPlay: ObjectId;
  players: Dic<PublicGameStatePlayer>;
  cardsOut: number;
  diffuseOut: number;
}

export interface GameState {
  turn: number;
  newTurn?: boolean;
  playerToPlay: ObjectId;
  players: Dic<GameStatePlayer>;
  cardsOut: number;
  diffuseOut: number;
}

// export interface PrivateGameStatePlayer {
//   id: ObjectId;
//   team: PlayerTeams;
//   numberOfCards: number;
// }

export interface PublicGameStatePlayer {
  id: ObjectId;
  numberOfCards: number;
}

export interface GameStatePlayer {
  id: ObjectId;
  team: PlayerTeams;
  numberOfCards: number;
  numerOfDiffuse: number;
  hasTheBomb: boolean;
}
