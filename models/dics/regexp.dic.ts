import { Dic } from '../interfaces';

export const RegExpDic: Dic<RegExp> = {
  password: new RegExp(
    '^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|]).{8,32}$'
  )
};
