export enum GameStatus {
  open = 'open',
  // starting = 'starting',
  inProgress = 'inProgress',
  // ending = 'ending',
  ended = 'ended',
  aborded = 'aborded'
}

export const IGameStatus = [
  'open',
  // 'starting',
  'inProgress',
  // 'ending',
  'ended',
  'aborded'
];
