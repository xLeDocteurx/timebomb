export enum APIS {
  back = 'back',
  socket = 'socket',
  auth = 'auth'
}

export enum APISMethods {
  post = 'post',
  get = 'get',
  put = 'put',
  delete = 'delete'
}
