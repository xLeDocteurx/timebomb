export enum CardsTypes {
  bomb = 'bomb',
  diffuse = 'diffuse',
  standard = 'standard'
}
