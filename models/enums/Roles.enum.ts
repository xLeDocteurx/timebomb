export enum Roles {
  god = 'god',
  admin = 'admin',
  registered = 'registered'
}

export const IRoles = ['god', 'admin', 'registered'];
