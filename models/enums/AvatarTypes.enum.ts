export enum AvatarTypes {
  gravatar = 'gravatar',
  identicon = 'identicon',
  monsterid = 'monsterid',
  // wavatar = 'wavatar',
  retro = 'retro',
  robohash = 'robohash'
}

export const IAvatarTypes = [
  'gravatar',
  'identicon',
  'monsterid',
  // 'wavatar',
  'retro',
  'robohash'
];
