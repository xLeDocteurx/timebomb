export enum GameTypes {
  short = 'short',
  correspondence = 'correspondence',
  ai = 'ai'
}

export const IGameTypes = ['short', 'correspondance', 'ai'];
