import * as crypto from './crypto';

(async () => {
  console.log('--------------------------');
  console.log('start');
  console.time('start');

  const input = 'Hello World !';
  const salt = 'foobar';
  // const iterations = 10000;
  const iterations = 100;

  console.log('--------------------------');
  console.log('input will be : ', input);
  console.log('salt will be : ', salt);

  console.log('--------------------------');
  console.log('Testing hash function');
  console.log('crypto.h(input) : ', crypto.h(input));
  console.log('crypto.h(input, crypto.HashAlgType.SHA1)', crypto.h(input, crypto.HashAlgType.SHA1));
  console.log('crypto.h(input, crypto.HashAlgType.SHA256)', crypto.h(input, crypto.HashAlgType.SHA256));
  console.log('crypto.h(input, crypto.HashAlgType.SHA384)', crypto.h(input, crypto.HashAlgType.SHA384));
  console.log('crypto.h(input, crypto.HashAlgType.SHA512)', crypto.h(input, crypto.HashAlgType.SHA512));

  console.log('--------------------------');
  console.log('Testing hmac function');
  console.log('crypto.hmac(input, salt) : ', crypto.hmac(input, salt));
  console.log("crypto.hmac(input, salt, 'md5') : ", crypto.hmac(input, salt, 'md5'));
  console.log("crypto.hmac(input, salt, 'sha1') : ", crypto.hmac(input, salt, 'sha1'));
  console.log("crypto.hmac(input, salt, 'sha256') : ", crypto.hmac(input, salt, 'sha256'));
  console.log("crypto.hmac(input, salt, 'sha512') : ", crypto.hmac(input, salt, 'sha512'));

  console.log('--------------------------');
  console.log('--------------------------');
  console.log('Testing hsecSync function');
  console.log('crypto.hsecSync(input, salt, iterations) : ', crypto.hsecSync(input, salt, iterations));
  console.log('crypto.hsecSync(input, salt, iterations, 8) : ', crypto.hsecSync(input, salt, iterations, 8));
  console.log('crypto.hsecSync(input, salt, iterations, 16) : ', crypto.hsecSync(input, salt, iterations, 16));
  console.log('crypto.hsecSync(input, salt, iterations, 32) : ', crypto.hsecSync(input, salt, iterations, 32));
  console.log('crypto.hsecSync(input, salt, iterations, 64) : ', crypto.hsecSync(input, salt, iterations, 64));

  console.log('--------------------------');
  console.log('--------------------------');
  console.log('Testing hsecAsync function');
  console.log('await crypto.hsecAsync(input, salt, iterations) : ', await crypto.hsecAsync(input, salt, iterations));
  console.log(
    'await crypto.hsecAsync(input, salt, iterations, 8) : ',
    await crypto.hsecAsync(input, salt, iterations, 8)
  );
  console.log(
    'await crypto.hsecAsync(input, salt, iterations, 16) : ',
    await crypto.hsecAsync(input, salt, iterations, 16)
  );
  console.log(
    'await crypto.hsecAsync(input, salt, iterations, 32) : ',
    await crypto.hsecAsync(input, salt, iterations, 32)
  );
  console.log(
    'await crypto.hsecAsync(input, salt, iterations, 64) : ',
    await crypto.hsecAsync(input, salt, iterations, 64)
  );

  console.log('--------------------------');
  console.log('Testing randomString function');
  console.log('crypto.randomString(16) : ', crypto.randomString(16));
  console.log('crypto.randomString(32) : ', crypto.randomString(32));

  console.log('--------------------------');
  console.log('Testing sym encryption decryption');

  let key: crypto.SymKey;
  // let encrypted: string;
  let encrypted;
  let decrypted: string;

  console.log('--------------------------');
  console.log('vanilla');
  key = new crypto.SymKey();
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('AES-CBC');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['AES-CBC'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('AES-CFB');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['AES-CFB'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('AES-CTR');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['AES-CTR'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('AES-ECB');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['AES-ECB'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('AES-OFB');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['AES-OFB'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('AES-CBC');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['DES-CBC'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('DES-ECB');
  key = new crypto.SymKey({ algType: crypto.CipherAlgType['DES-ECB'] });
  key.generateKey();
  console.log('key', key);
  console.log('key.pem', key.pem);
  encrypted = await key.encrypt(input);
  console.log('encrypted : ', encrypted);
  decrypted = await key.decrypt(encrypted);
  console.log('decrypted : ', decrypted);

  console.log('--------------------------');
  console.log('Testing asym encryption decryption');

  let keypair: crypto.KeyPair;
  let encryptedAsym: string;
  let decryptedAsym: string;

  console.log('--------------------------');
  console.log('vanilla');
  keypair = new crypto.KeyPair();
  await keypair.generateKeyPair();
  encryptedAsym = await keypair.encrypt(input);
  console.log('encryptedAsym : ', encryptedAsym);
  decryptedAsym = await keypair.decrypt(encryptedAsym);
  console.log('decryptedAsym : ', decryptedAsym);

  console.log('--------------------------');
  console.timeEnd('start');
  console.log('--------------------------');
})();
