import { AvatarTypes } from '../models/enums';
import { PrivateUser, PublicUser } from '../models/interfaces';

const tokenCookieName = 'token';
const userCookieName = 'user';

function getGravatarUrl(user: PrivateUser | PublicUser) {
  if (user.avatarType === AvatarTypes.gravatar) {
    return `https://www.gravatar.com/avatar/${user.gravatarHash}`;
  } else {
    return `https://www.gravatar.com/avatar/${user.gravatarHash}?&d=${user.avatarType}`;
  }
}

function saveUserAsCookie(user) {
  const stringifiedUser = JSON.stringify(user);
  const days = 30;
  let date = new Date();
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
  const expires = `expires=${date.toUTCString()}`;
  document.cookie = `${userCookieName}=${stringifiedUser};${expires};path=/`;
}

function getUserAsCookie() {
  const name = `${userCookieName}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return JSON.parse(c.substring(name.length, c.length));
    }
  }
  // return ""
  return null;
}

function saveTokenAsCookie(token) {
  const days = 30;
  let date = new Date();
  date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
  const expires = `expires=${date.toUTCString()}`;
  document.cookie = `${tokenCookieName}=${token};${expires};path=/`;
}

function getTokenAsCookie() {
  const name = `${tokenCookieName}=`;
  const decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  // return ""
  return null;
}

function deleteAllCookies() {
  document.cookie = `${tokenCookieName}=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/;`;
  document.cookie = `${userCookieName}=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/;`;
}

export const session = {
  getGravatarUrl,
  saveUserAsCookie,
  getUserAsCookie,
  saveTokenAsCookie,
  getTokenAsCookie,
  deleteAllCookies
};
