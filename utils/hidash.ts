import { randomInt } from './randomInt';

export function clone<T>(obj: T): T {
  const clonedObj = Object.assign({}, obj);
  return clonedObj;
}

export function cloneArray(arr: any[]): any[] {
  const clonedArr = [...arr];
  return clonedArr;
}

export function merge<O>(...args): O {
  return Object.assign({}, ...args) as any;
}

export function mergeArrays<O>(...args: any[]): O[] {
  return [].concat(...args) as any;
}

export function omit<T, O>(obj: T, props: string[]): O {
  const clonedObj = clone(obj);
  props.forEach(prop => {
    delete clonedObj[prop];
  });
  return clonedObj as any;
}

export function pick<T, O>(obj: T, props: string[]): O {
  const clonedObj = clone(obj);
  const dummyObj = {};
  props.forEach(prop => {
    dummyObj[prop] = clonedObj[prop];
  });
  return clonedObj as any;
}

export function exchange<T>(array: T[], iIndex: number, jIndex: number): T[] {
  let clonedArray = cloneArray(array);
  clonedArray.splice(iIndex, 1, array[jIndex]);
  clonedArray.splice(jIndex, 1, array[iIndex]);
  return clonedArray;
}

// https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
export function shuffle<T>(array: T[]): T[];
export function shuffle<T>(array: T[], times: number): T[];
export function shuffle<T>(array: T[], times?: number): T[] {
  if (times) {
    let clonedArray = cloneArray(array);
    for (let i = 0; i < times; i++) {
      clonedArray = shuffle(clonedArray);
    }
    return clonedArray;
  } else {
    let clonedArray = cloneArray(array);
    const n = array.length;
    // Reserved shuffle
    // for (let i = n - 1; i > 0; i--) {
    //   const j = randomInt(0, i)
    //   clonedArray = exchange(clonedArray, i, j);
    // }
    for (let i = 0; i < n - 2; i++) {
      const j = randomInt(i, n - 1);
      clonedArray = exchange(clonedArray, i, j);
    }
    return clonedArray;
  }
}

export function badShuffle<T>(array: T[]): T[] {
  const clonedArray = cloneArray(array);
  return clonedArray.sort((a, b) => 0.5 - randomInt());
}
