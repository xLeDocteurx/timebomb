import { merge } from './hidash';

export function get(obj: any, path: string) {
  const pathElements = path.split('.');
  return pathElements.reduce((a, b) => {
    return (a = a[b]);
  }, obj);
}

export function set(obj: any, path: string, value: any): object {
  const pathElements = path.split('.').reverse();
  const dummyObject = pathElements.reduce((a, b) => {
    return { [b]: a };
  }, value);
  return merge(obj, dummyObject);
  // return Object.assign({}, obj, dummyObject);
}

export const objectPath = { get, set };
