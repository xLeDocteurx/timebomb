export function randomInt(): number;
export function randomInt(max: number): number;
export function randomInt(min: number, max: number): number;
export function randomInt(minOrMax?: number, max?: number): number {
  // if (minOrMax) minOrMax = Math.floor(minOrMax);
  // if (max) max = Math.floor(max);
  if (minOrMax !== undefined && max !== undefined) {
    return Math.round(Math.random() * (max - minOrMax)) + minOrMax;
  } else if (minOrMax !== undefined) {
    return Math.round(Math.random() * minOrMax);
  } else {
    return Math.random();
  }
}
