import * as forge from 'node-forge';
import { SymKey } from './classes';
import { BlockLength, CipherAlgType, HashAlgType } from './enums';

export * from './classes';
export * from './enums';

export function randomString(length: number = 32): string {
  return forge.util.bytesToHex(forge.random.getBytesSync(length));
}

export function h(input: any, alg: HashAlgType = HashAlgType.SHA256): string {
  if (alg === HashAlgType.SHA1) {
    var md = forge.md.sha1.create();
    md.update(input);
    return md.digest().toHex();
  } else if (alg === HashAlgType.SHA256) {
    var md = forge.md.sha256.create();
    md.update(input);
    return md.digest().toHex();
  } else if (alg === HashAlgType.SHA384) {
    var md = forge.md.sha384.create();
    md.update(input);
    return md.digest().toHex();
  } else if (alg === HashAlgType.SHA512) {
    var md = forge.md.sha512.create();
    md.update(input);
    return md.digest().toHex();
  }
  // else {
  //   throw 'invalid HashAlgType';
  // }
}

export function hmac(input: any, salt: string, alg: forge.hmac.Algorithm = 'sha256'): string {
  var hmac = forge.hmac.create();
  hmac.start(alg, salt);
  hmac.update(input);
  return hmac.digest().toHex();
}

export function hsecSync(input: any, salt: string, iterations: number | string, blockLength: BlockLength = 64): string {
  if (typeof iterations === 'string') {
    iterations = Number(iterations);
  }
  return forge.util.bytesToHex(forge.pkcs5.pbkdf2(input, salt, iterations, blockLength));
}

export async function hsecAsync(
  input: string,
  salt: string,
  iterations: number | string,
  blockLength: BlockLength = 64
): Promise<string> {
  console.log('hsecAsync : ');
  console.log('input', input);
  console.log('iterations', iterations);
  console.log('blockLength', blockLength);
  if (typeof iterations === 'string') {
    iterations = Number(iterations);
  }
  return new Promise((resolve, reject) => {
    forge.pkcs5.pbkdf2(input, salt, iterations as number, blockLength, (err, derivedKey) => {
      if (err) {
        reject(err);
      } else {
        resolve(forge.util.bytesToHex(derivedKey));
      }
    });
  });
}

export async function symEncrypt(
  input: string,
  key: string,
  algType: CipherAlgType = CipherAlgType['AES-CBC']
): Promise<string> {
  const symkey = new SymKey({ pem: key, algType });
  return symkey.encrypt(input);
}

export async function symDecrypt(
  input: string,
  key: string,
  algType: CipherAlgType = CipherAlgType['AES-CBC']
): Promise<string> {
  const symkey = new SymKey({ pem: key, algType });
  return symkey.decrypt(input);
}

export function asymEncrypt(
  input: string,
  publicKeyPem: string,
  algType: forge.pki.rsa.EncryptionScheme = 'RSAES-PKCS1-V1_5'
): string {
  const publicKey = forge.pki.publicKeyFromPem(publicKeyPem);
  return forge.util.encode64(publicKey.encrypt(JSON.stringify(input), algType));
}

export function asymDecrypt(
  input: string,
  privateKeyPem: string,
  algType: forge.pki.rsa.EncryptionScheme = 'RSAES-PKCS1-V1_5'
): string {
  const privateKey = forge.pki.privateKeyFromPem(privateKeyPem);
  return JSON.parse(privateKey.decrypt(forge.util.decode64(input), algType));
}
