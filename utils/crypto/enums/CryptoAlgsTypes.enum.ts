export type BlockLength = 8 | 16 | 32 | 64;

export enum HashAlgType {
  SHA1 = 'SHA1',
  SHA256 = 'SHA256',
  SHA384 = 'SHA384',
  SHA512 = 'SHA512'
}

export enum CipherAlgType {
  'AES-ECB' = 'AES-ECB',
  'AES-CBC' = 'AES-CBC',
  'AES-CFB' = 'AES-CFB',
  'AES-OFB' = 'AES-OFB',
  'AES-CTR' = 'AES-CTR',
  'DES-ECB' = 'DES-ECB',
  'DES-CBC' = 'DES-CBC'
}

export enum PKIAlgType {
  'NONE' = 'NONE',
  'RAW' = 'RAW',
  'RSA-OAEP' = 'RSA-OAEP',
  'RSAES-PKCS1-V1_5' = 'RSAES-PKCS1-V1_5'
}
