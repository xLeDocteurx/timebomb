import * as forge from 'node-forge';
import { KeyLengthTypes, PKIAlgType } from '../enums';

const separator = '$';

export class KeyPair {
  private _keypair: forge.pki.rsa.KeyPair;

  private _algType: PKIAlgType;

  constructor(
    options?: { algType?: PKIAlgType }
    // keyPair?: { publicKey: string; privateKey: string }
  ) {
    // if (keyPair) {
    //   this._privateKey = new PrivateKey(algType + separator + keyPair.privateKey);
    //   this._publicKey = new PublicKey(algType + separator + keyPair.publicKey);
    // }
    this._algType = options?.algType ?? PKIAlgType['RSAES-PKCS1-V1_5'];
  }

  get privateKey() {
    return this._keypair.privateKey;
  }

  get privateKeyPem() {
    return forge.pki.privateKeyToPem(this._keypair.privateKey);
  }

  get publicKey() {
    return this._keypair.publicKey;
  }

  get publicKeyPem() {
    return forge.pki.publicKeyToPem(this._keypair.publicKey);
  }

  async ifNoKeyPairGenerateThem(): Promise<void> {
    if (!this._keypair) {
      await this.generateKeyPair();
    }
  }

  async generateKeyPair(
    algType: PKIAlgType = PKIAlgType['RSAES-PKCS1-V1_5'],
    keyLength: KeyLengthTypes = 32
  ): Promise<void> {
    const keypair = await new Promise<forge.pki.rsa.KeyPair>((resolve, reject) => {
      forge.pki.rsa.generateKeyPair({ workers: -1 }, (err, keypair) => {
        if (err) {
          reject(err);
        } else {
          resolve(keypair);
        }
      });
    });
    this._keypair = keypair;
  }

  async encrypt(input: string) {
    await this.ifNoKeyPairGenerateThem();
    return forge.util.encode64(this._keypair.publicKey.encrypt(JSON.stringify(input), this._algType));
  }

  async decrypt(input: string) {
    await this.ifNoKeyPairGenerateThem();
    return JSON.parse(this._keypair.privateKey.decrypt(forge.util.decode64(input), this._algType));
  }

  async sign(input: any) {
    await this.ifNoKeyPairGenerateThem();
    return await this._keypair.privateKey.sign(input, this._algType);
  }

  async verify(input: any, signature: string) {
    await this.ifNoKeyPairGenerateThem();
    return await this._keypair.publicKey.verify(input, signature, this._algType);
  }
}
