import * as forge from 'node-forge';
import { CipherAlgType, KeyLengthTypes } from '../enums';

const separator = '$';
const ivLength = 32;

export class SymKey {
  private _algType: CipherAlgType;
  private _key: string;

  constructor(options?: { algType?: CipherAlgType; pem?: string }) {
    if (options?.pem) {
      console.log('SymKey class');
      console.log('options', options);
      this._key = options.pem;
    }
    this._algType = options?.algType ?? CipherAlgType['AES-CBC'];
  }

  get pem(): string {
    return this._key;
  }

  async ifNoKeyGenerateIt(): Promise<void> {
    if (!this._key) {
      await this.generateKey();
    }
  }

  generateKey(keyLength: KeyLengthTypes = 16) {
    this._key = forge.util.bytesToHex(forge.random.getBytesSync(keyLength));
  }

  async encrypt(input: string): Promise<string> {
    console.log('symkey encrypt');
    await this.ifNoKeyGenerateIt();
    console.log('this._algType', this._algType);
    console.log('this._key', this._key);
    const cipher = forge.cipher.createCipher(this._algType, this._key);
    const iv = forge.util.bytesToHex(forge.random.getBytesSync(ivLength));
    console.log('iv : ', iv);
    console.log('input : ', input);
    cipher.start({ iv: iv });
    cipher.update(forge.util.createBuffer(input));
    cipher.finish();
    // const encrypted = cipher.output;
    // return encrypted.toHex();
    console.log('cipher.output.toHex() : ', cipher.output.toHex());
    console.log('iv + separator + cipher.output.toHex() : ', iv + separator + cipher.output.toHex());
    return iv + separator + cipher.output.toHex();
  }

  async decrypt(input: string): Promise<string> {
    await this.ifNoKeyGenerateIt();
    const decipher = forge.cipher.createDecipher(this._algType, this._key);
    const iv = input.split(separator)[0];
    const content = input.split(separator)[1];

    decipher.start({ iv: iv });
    decipher.update(forge.util.createBuffer(forge.util.hexToBytes(content)));
    // const result = decipher.finish();
    decipher.finish();
    return decipher.output.toString();
  }
}
