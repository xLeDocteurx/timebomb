export const Charsets = {
  alphaDec: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  alpha: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
  lowerAlpha: 'abcdefghijklmnopqrstuvwxyz',
  upperAlpha: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  dec: '0123456789',
  binary: '01',
  octa: '01234567',
  hexa: '0123456789abcdef'
};

export type Set = keyof typeof Charsets;

export function generateString(len: number | string, sets: Set | Set[] = 'alphaDec') {
  if (typeof len === 'string') {
    len = Number(len);
  }
  const chars: string =
    typeof sets === 'string'
      ? Charsets[sets]
      : sets.reduce((acc, set) => {
          for (let i = 0; i < Charsets[set].length; i++) {
            if (!acc.includes(Charsets[set][i])) {
              acc += Charsets[set][i];
            }
          }
          return acc;
        }, '');
  let str = '';

  for (var i = 0; i < len; i++) {
    str += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return str;
}
