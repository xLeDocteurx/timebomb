import { util } from 'node-forge';

export function xor(a: string, b: string) {
  const length = Math.max(a.length, b.length);
  return util.bytesToHex(util.xorBytes(util.hexToBytes(a), util.hexToBytes(b), Math.max(length))).substr(0, length);
}
